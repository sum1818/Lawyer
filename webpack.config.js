'use strict';

const path = require('path');
const autoprefixer = require('autoprefixer');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');

const isProd = process.env.NODE_ENV === 'production';
const minPostfix = isProd ? '.min' : '';
const minify = isProd ? 'minimize' : '';
const hash = '[hash:7]';

const entry = ['whatwg-fetch', 'babel-polyfill', './app/js/entry.js'];
const devEntry = [
  'webpack/hot/dev-server',
  'webpack-hot-middleware/client?reload=true'
].concat(entry);
const basePlugins = [
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
    }
  }),
  new webpack.optimize.CommonsChunkPlugin({
    names: ['vendor']
  }),
  new HTMLWebpackPlugin({
    title: '律师在线',
    template: 'app/index.html',
    // inject: false,
    prod: isProd,
    minify: isProd ? {
      removeComments: true,
      collapseWhitespace: true
    } : null,
  })
];
const envPlugins = isProd ? [
  new ExtractTextPlugin(`css/style.${hash}${minPostfix}.css`, {
    allChunks: true
  }),
  new webpack.optimize.UglifyJsPlugin({
    output: {
      comments: false,  // remove all comments
    },
    compress: {
      warnings: false
    }
  }),
  new webpack.BannerPlugin(`build: ${new Date().toString()}`)
] : [
  new webpack.optimize.OccurenceOrderPlugin(),
  new webpack.HotModuleReplacementPlugin(),
  // @see https://www.npmjs.com/package/eslint-loader#noerrorsplugin
  new webpack.NoErrorsPlugin(),
];

const distEntry = {};
distEntry['app'] = isProd ? entry : devEntry;
distEntry['vendor'] = ['./app/js/thirdparty/pinchzoom.js'];

module.exports = {
  debug: !isProd,
  devtool: !isProd ? '#eval' : null,

  entry: distEntry,

  output: {
    path: path.join(__dirname, 'dist'),
    filename:  "js/[name].[hash].js",
    publicPath: '/'
  },

  externals: {
    moment: true
  },

  devServer: {
    proxy: {
      '/rest/*': {
        target: 'https://app.nagulawyer.xin/rest',
        changeOrigin: true,
        secure: false
      }
    }
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loaders: [
          'babel',
          // 'eslint',
        ],
        include: [
          path.join(__dirname, 'app/js'),
          path.resolve(__dirname, 'node_modules/amazeui-touch/js'),
        ]
      },
      {
        test: /\.scss/,
        loader: isProd ? ExtractTextPlugin.extract(
          'style',
          `css?${minify}!postcss!sass`
        ) : 'style!css?sourceMap!postcss!sass?sourceMap',
      },
      {
        test: /\.jpe?g$|\.gif$|\.png|\.ico$/,
        loaders: [
          // 'file?name=[path][name].[ext]&context=app',
          'url?limit=8192&context=app&name=[path][name].[ext]'
          // 'image-webpack'
        ]
      },
      {
        test: /\.txt$|\.json$|\.webapp$/,
        loader: 'file?name=[path][name].[ext]&context=app'
      },
      {
        test: /\.svg$/,
        loader: 'url?mimetype=image/svg+xml&name=[name].[ext]'
      },
      {
        test: /\.woff$/,
        loader: 'url?mimetype=application/font-woff&name=[name].[ext]'
      },
      {
        test: /\.woff2$/,
        loader: 'url?mimetype=application/font-woff2&name=[name].[ext]'
      },
      {
        test: /\.[ot]tf$/,
        loader: 'url?mimetype=application/octet-stream&name=[name].[ext]'
      },
    ]
  },

  plugins: basePlugins.concat(envPlugins),

  // global mode
  // externals: {
  //   'react': 'React',
  //   'react-dom': 'ReactDOM',
  //   'react-addons-css-transition-group': ['React', 'addons', 'CSSTransitionGroup'],
  //   'amazeui-touch': 'AMUITouch',
  // },

  // loader config
  postcss: [autoprefixer({browsers: ['> 1%', 'last 2 versions', 'ie 10']})],

  // @see https://www.npmjs.com/package/image-webpack-loader
  imageWebpackLoader: {},

  watch: !isProd

};
