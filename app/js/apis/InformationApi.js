/**
 * Created by zl on 2017/5/9.
 */
import {getApi,getApiAssets,buildParams} from "./Api";

export function getInformationContent(id) {
  return getApiAssets(`/information/${id}.html`);
}

export function getUrlContent(url) {
  return getApiAssets(url);
}

export function getIndexInfos() {
  return getApi("/rest/v1/public/indexInfos");
}
