/**
 * Created by zl on 2017/4/9.
 */
import {getApi, putApi, postApi, fileApi, buildParams} from "./Api";

export function queryLawyers(name) {
  return getApi("/rest/v1/public/lawyers?" + buildParams({name}));
}

export function lawyerDetail(id) {
  return getApi("/rest/v1/public/lawyer?" + buildParams({id}));
}

export function getIndexLawyers() {
  return getApi("/rest/v1/public/indexlawyers");
}

export function getPaySign(appId, timeStamp, nonceStr, packageStr) {
  return postApi("/rest/v1/weixin/paysign", {appId, timeStamp, nonceStr, "package": packageStr});
}

