/**
 * Created by zl on 2017/3/7.
 */
import {getApi, putApi, postApi, fileApi, buildParams} from "./Api";
import * as XUtil from "../utils/XUtil";

export function getUserInfo() {
  return getApi("/rest/v1/user/detail").then(data => {
    if (data.code == 0) {//已经登录
      XUtil.setItem("isLogin", true);
    } else {//没有登录
      XUtil.setItem("isLogin", false);
    }
    return data;
  });
}

export function sendLoginCode(mobile) {
  return postApi("/rest/v1/code/login", {mobile});
}

export function sendLogin(mobile, code, wxcode) {
  return postApi("/rest/v1/auth/login", {mobile, code, wxcode}).then(data => {
    if (data.code == 0) {
      XUtil.setItem("isLogin", true);
    }
    return data;
  });
}

export function modifyName(name) {
  return putApi("/rest/v1/user/name", {name});
}

export function sendLogout() {
  return getApi("/rest/v1/user/logout").then(data => {
    if (data.code == 0) {
      XUtil.setItem("isLogin", false);
    }
    return data;
  });
}

export function sendRebindCode() {
  return postApi("/rest/v1/code/user/rebind", {});
}

export function sendResetCode() {
  return postApi("/rest/v1/code/user/reset", {});
}

export function rebindMobile(code) {
  return putApi("/rest/v1/user/rebind", {code});
}

export function sendBindCode(mobile) {
  return postApi("/rest/v1/code/bind", {mobile});
}

export function bindMobile(mobile, code) {
  return putApi("/rest/v1/user/bind", {mobile, code});
}

export function isValidRefer(id) {
  return getApi("/rest/v1/user/validation?" + buildParams({id}))
}

export function getReferInfo(referId) {
  return getApi("/rest/v1/user/refer/detail?" + buildParams({referId}));
}

export function modifyRemark(referId, remark) {
  return putApi("/rest/v1/user/refer/remark", {referId, remark});
}

export function resetPass(code, pass) {
  return putApi("/rest/v1/user/pass", {code, pass});
}

export function uploadHead(headFile) {
  return fileApi("/rest/v1/user/head", [headFile]);
}

export function addConsult(consultFiles, content) {
  return fileApi("/rest/v1/user/consult", consultFiles||[], {content})
}

export function listConsults(lastId) {
  return getApi("/rest/v1/user/consults?" + buildParams({lastId}));
}

export function buyVip(type,paymethod,openId) {
  return postApi("/rest/v1/user/viporder", {type,paymethod,openId});
}

export function updateVipOrder(id, orderNumber) {
  return putApi("/rest/v1/user/viporder", {id, orderNumber});
}

export function vipState() {
  return getApi("/rest/v1/user/vipstate");
}

export function postRecommend(recommend) {
  return postApi("/rest/v1/user/recommend", {recommend});
}

export function queryOrder(orderNo) {
  return getApi("/rest/v1/user/orderQuery?" +  buildParams({orderNo}));
}

export function activeVipCard(cardNumber, userName, tel, address) {
  return putApi("/rest/v1/user/vipstate", {cardNumber, userName, tel, address});
}

export function getOrder(orderNo) {
  return getApi("/rest/v1/user/order?" +  buildParams({orderNo}));
}
