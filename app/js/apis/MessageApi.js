/**
 * Created by zl on 2017/4/9.
 */
import {getApi, postApi, buildParams} from "./Api";

export function sendPrivateMessage(referId, message) {
  return postApi("/rest/v1/message/private", {referId, message});
}

export function privateMessageList(referId, messageId) {
  return getApi("/rest/v1/message/list?" + buildParams({referId, messageId}));
}

export function chatList() {
  return getApi("/rest/v1/message/chats");
}

export function getUnreadCount(conversationType, referId) {
  return new Promise((resolve, reject) => {
    resolve({referId, count:0});
  });
}

export function clearUnReadCount(conversationType, referId) {
  return new Promise((resolve, reject) => {
    resolve({referId, isClear:true});
  });
}

export function getTotalUnreadCount() {
  return new Promise((resolve, reject) => {
    resolve({count:0});
  });
}
