/**
 * Created by zl on 2017/4/9.
 */
import {postApi,putApi, getApi, buildParams} from "./Api";

export function addLawyer(name,mobile,years,area,specialty,synopsis) {
  return postApi("/rest/v1/admin/lawyer" , {name,mobile,years,area,specialty,synopsis});
}

export function modifyLawyer(lawyerId,name,mobile,years,area,specialty,synopsis) {
  return putApi("/rest/v1/admin/lawyer" , {lawyerId,name,mobile,years,area,specialty,synopsis});
}

export const getConsults = (account, page, size) => getApi("/rest/v1/assessor/consults?" + buildParams({account, page, size}));

export const putReply = (id, reply) => putApi("/rest/v1/assessor/consult", {id, reply});
