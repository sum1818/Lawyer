/**
 * Created by zl on 2017/3/7.
 */
import {getApi, putApi, postApi, fileApi, buildParams} from './Api';

export function getGuestList() {
  return getApi("/rest/v1/lawyer/guest/list");
}

export function searchGuest(name) {
  return getApi("/rest/v1/lawyer/guest/search?" + buildParams({name}));
}

export function addGuest(mobile, remark) {
  return postApi("/rest/v1/lawyer/guest/add", {mobile,remark});
}

export function addCase(data) {
  return postApi("/rest/v1/lawyer/case/add", data);
}

export function modifyCase(data, id) {
  return putApi("/rest/v1/lawyer/case/modify?" + buildParams({id}), data)
}

export function getCaseList(finished, lastId) {
  return getApi("/rest/v1/lawyer/case/list?" + buildParams({finished, lastId}))
}

export function caseDetail(id) {
  return getApi("/rest/v1/lawyer/case/detail?" + buildParams({id}));
}

export function uploadSessionFiles(files) {
  return fileApi("/rest/v1/lawyer/case/files?" + buildParams({type:"session"}), files);
}

export function searchCase(name) {
  return getApi("/rest/v1/lawyer/case/search?" + buildParams({name}));
}

export function conflickCheck(caseId, casenum) {
  return getApi("/rest/v1/lawyer/case/check?" + buildParams({caseId,casenum}));
}
