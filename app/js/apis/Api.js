/**
 * Created by zl on 2017/3/8.
 */

import * as XUtil from "../utils/XUtil";

export const buildParams = (data) => {
  let params = [];
  for(let key of Object.getOwnPropertyNames(data)) {
    let value = data[key];
    if (value !== null && value !== undefined) {
      params.push(key + "=" + value);
    }
  }
  return params.join("&");
};

/**
 * Response处理未登录
 * @returns {function(*)}
 */
const handleResponse = () => {
  return data => {
    if (data.code == -100) {//没有登录,跳转到登录页面
      //使用replace跳转
      XUtil.replaceLogin();
    }
    return data;
  }
};

/**
 * GET请求
 * @param url
 * @returns {Promise.<TResult>}
 */
export function getApi(url) {
  if (url.indexOf("/user/detail") > -1) {//如果是用户详情的话就不要拦截了
    return fetch(url,{ method: 'GET', credentials: 'include'}).then(response => response.json());
  } else {
    return fetch(url,{method: 'GET', credentials: 'include'}).then(response => response.json()).then(handleResponse());
  }
}

/**
 * GET请求, 返回原始结果
 * @param url
 * @param host
 * @returns {Promise.<TResult>}
 */
export function getApiRaw(url,host="https://assets.nagulawyer.xin/") {
  return fetch(host+url,{method: 'GET', credentials: 'include'}).then(response => response.text());
}


/**
 * 获取资源
 * @param url
 * @param host
 * @return {Promise.<TResult>}
 */
export function getApiAssets(url, host = "https://assets.nagulawyer.xin/") {
  return fetch(host+url+"?v="+new Date().valueOf(),{method: 'GET'}).then(response => response.text());
}

/**
 * PUT请求
 * @param url
 * @param data
 * @returns {Promise.<TResult>}
 */
export function putApi(url, data) {
  return fetch(url, { method: 'PUT', credentials: 'include', headers: { 'Content-Type': 'application/x-www-form-urlencoded'}, body: buildParams(data)}).then(response => response.json()).then(handleResponse());
}

/**
 * POST请求
 * @param url
 * @param data
 * @returns {Promise.<TResult>}
 */
export function postApi(url, data) {
  return fetch(url, { method: 'POST', credentials: 'include', headers: { 'Content-Type': 'application/x-www-form-urlencoded'}, body: buildParams(data)}).then(response => response.json()).then(handleResponse());
}

/**
 * 文件上传请求
 * @param url
 * @param files
 * @param data
 * @return {Promise.<TResult>}
 */
export function fileApi(url, files, data) {
  let formData = new FormData();
  if (files != null && files.length > 0) {
    files.forEach(file => {
      formData.append("file", file);
    });
  }
  if (data) {
    for (let i in data) {
      if (data.hasOwnProperty(i)) {
        formData.append(i, data[i]);
      }
    }
  }
  return fetch(url, { method: 'POST', credentials: 'include', body: formData}).then(response => response.json()).then(handleResponse());
}
