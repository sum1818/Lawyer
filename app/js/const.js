/**
 * Created by zl on 2017/3/31.
 */

export const partyTypes = [
  "原告",
  "被告",
  "第三人",
  "其它"
];

export const caseStates = {
  "0": "已收案",
  "1": "一审",
  "2": "二审",
  "3": "执行",
  "20": "结案"
};

export const sessionTypes = [
  "一审",
  "二审",
  "仲裁",
  "其它"
];

export const caseTypes = [
  "民事案件",
  "刑事案件"
];

export const visibleTypes = [
  "只有自己",
  "全体律师",
  "注册用户",
  "全体用户"
];

/**资源路径**/
export const ASSETS_HOST = "https://assets.nagulawyer.xin/";
