/**
 * Created by zl on 2017/3/4.
 */
import * as ActionType from './ActionType';

export function showTab(show) {
  return {
    type: ActionType.SHOW_TAB,
    show
  }
}

export function setTitle(title) {
  return {
    type: ActionType.SET_TITLE,
    title
  }
}

export function setMenu(title, icon, action) {
  if (action) {
    return {
      type: ActionType.SET_MENU,
      page: this,
      title,
      icon,
      action
    }
  } else {
    return {
      type: ActionType.SET_MENU,
      page: this,
      title,
      icon
    }
  }
}

export function showTips(title, content) {
  let ret = {
    type: ActionType.DIALOG_TIP_SHOW,
    isOpen:true
  };
  if (title) ret.title = title;
  if (content) ret.children = content;
  return ret;
}

export function hideTips() {
  return {
    type: ActionType.DIALOG_TIP_SHOW,
    isOpen:false
  }
}

export function showAlert(title, content) {
  let ret = {
    type: ActionType.DIALOG_ALERT_SHOW,
    isOpen:true
  };
  if (title) ret.title = title;
  if (content) ret.children = content;
  return ret;
}

export function hideAlert() {
  return {
    type: ActionType.DIALOG_ALERT_SHOW,
    isOpen:false
  }
}

export function showPrompt(title,action) {
  let ret = {
    type: ActionType.DIALOG_PROMPT_SHOW,
    isOpen:true
  };
  if (title) ret.title = title;
  if (action) ret.action = action;
  return ret;
}

export function hidePrompt() {
  return {
    type: ActionType.DIALOG_PROMPT_SHOW,
    isOpen:false
  }
}

export function sendParams(params) {
  return Object.assign({type: ActionType.PRAMS_TRANS}, params);
}

export function saveCache(data) {
  return {type: ActionType.SAVE_CACHE, data};
}

export function delCache(data) {
  return {type: ActionType.DEL_CACHE, data};
}

export function addMessage(targetId, message) {
  return {type: ActionType.ADD_MESSAGE, targetId, message};
}

export function insertMessage(targetId, messageList) {
  return {type: ActionType.INSERT_MESSAGE_LIST, targetId, messageList};
}

export function initIM() {
  return {type: ActionType.INIT_IM};
}

export function addUnreadCount(referId, msg) {
  return {type: ActionType.ADD_UNREAD_COUNT, referId, msg};
}
