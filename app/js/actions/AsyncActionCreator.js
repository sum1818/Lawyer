/**
 * Created by zl on 2017/3/7.
 */
import { GET_DATA_SUCCESS, GET_DATA_FAILED } from './ActionType';
import * as userApi from '../apis/UserApi';

const next = (promise, dispatch) => {
  promise.then(json => {
    dispatch({
      type: GET_DATA_SUCCESS,
      data: json
    })
  }).catch(error => {
    dispatch({
      type: GET_DATA_FAILED,
      error: error
    })
  });
};
/**
 * 获取用户信息
 * @returns {Function}
 */
export function getUserInfoAction() {
  return dispatch => next(userApi.getUserInfo(), dispatch);
}

/**
 * 发送登录验证码
 */
export function sendLoginCodeAction(mobile) {
  return dispatch => next(userApi.sendLoginCode(mobile), dispatch);
}
