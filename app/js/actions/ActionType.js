/**
 * Created by zl on 2017/3/4.
 */
export const SHOW_TAB = 'show'
export const SET_TITLE = 'title';
export const SET_MENU = 'menu';
export const GET_DATA_SUCCESS = 'api_success';
export const GET_DATA_FAILED = 'api_failed';
export const DIALOG_TIP_SHOW = 'dialog_tip_show';
export const DIALOG_ALERT_SHOW = 'dialog_alert_show';
export const DIALOG_PROMPT_SHOW = 'dialog_prompt_show';
export const PRAMS_TRANS = 'params_trans';
export const SAVE_CACHE = 'save_cache';
export const DEL_CACHE = 'del_cache';
export const ADD_MESSAGE = 'add_msg';
export const INSERT_MESSAGE_LIST = 'insert_msg_list';
export const INIT_IM = 'init_im';
export const ADD_UNREAD_COUNT = 'add_unread_count';
