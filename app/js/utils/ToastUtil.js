/**
 * Created by zl on 2017/3/17.
 */
import nativeToast from "native-toast";

export default function showToast(msg) {
  nativeToast({message: msg, position: 'bottom', timeout: 2000, type: 'tip'});
}
