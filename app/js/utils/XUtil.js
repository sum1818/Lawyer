/**
 * Created by zl on 2017/3/10.
 */

const storage = {};

export function isMobile(mobile) {
  return /^\d{11}$/.test(mobile);
}

export function isNumber(number) {
  return /^\d+$/.test(number);
}

export function isValidLoginCode(code) {
  return /^\d{6}$/.test(code);
}

export function isEmpty(val) {
  return !(val && String(val).trim().length > 0)
}

/**
 * 对Date的扩展，将 Date 转化为指定格式的String
 * @param  {Date}       日期
 * @return {String}     字符串格式
 */
export function convertDate(date, format) {
  let str = format;
  const o = {
    'M+': date.getMonth() + 1,
    'D+': date.getDate(),
    'h+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds(),
  };
  if (/(Y+)/.test(format)) {
    str = str.replace(RegExp.$1,
      (date.getFullYear().toString()).substr(4 - RegExp.$1.length));
  }

  for (const k in o) { // eslint-disable-line
    if (new RegExp(`(${k})`).test(format)) {
      str = str.replace(RegExp.$1,
        (RegExp.$1.length === 1) ?
          o[k] : (`00${o[k]}`.substr((o[k].toString()).length)));
    }
  }

  return str;
}

function throwIfInvalidDate(date) {
  if (Object.prototype.toString.call(date, null) !== '[object Date]') {
    throw new Error('参数类型不对');
  }
}

export function nextDate(now, index = 0) {
  throwIfInvalidDate(now);
  const date = new Date(now.getFullYear(), now.getMonth(), now.getDate() + index);
  return date;
}

/**
 * 压缩图片
 * @param image
 * @param width
 * @param height
 * @returns {*}
 */
export function compressImage(image, width=1920, height=1920) {
  let canvas = document.createElement("canvas");
  let ctx = canvas.getContext('2d');
  let w = image.naturalWidth;
  let h = image.naturalHeight;
  let rate = w / h;
  if (w > width) {//最大不能超过1920像素
    w = width;
    h = w / rate;
  }
  if (h > height) {//最大不能超过1080像素
    h = height;
    w = h * rate;
  }
  canvas.width = w;
  canvas.height = h;
  ctx.drawImage(image, 0, 0, w, h, 0, 0, w, h);
  let data = canvas.toDataURL("image/jpeg", 0.85);
  data = data.split(',')[1];
  data = window.atob(data);
  let ia = new Uint8Array(data.length);
  for (let i = 0; i < data.length; i++) {
    ia[i] = data.charCodeAt(i);
  }
  return new Blob([ia], {
    type: "image/jpeg"
  });
}

/**
 * 是否是正确的密码
 * @param password
 * @return {boolean}
 */
export function isValidPass(password) {
  return /^[a-zA-Z\d]{6,16}$/.test(password);
}

/**
 * 聊天时间
 * @param sentTime
 * @return {String}
 */
export function messageDate(sentTime) {
  let day1 = new Date().valueOf() / 86400000;
  let day2 = sentTime / 86400;
  if (parseInt(day1) == parseInt(day2)) {
    return convertDate(new Date(sentTime * 1000), "hh:mm");
  } else {
    return convertDate(new Date(sentTime * 1000), "MM月DD日 hh:mm");
  }
}

/**
 * 保存状态
 * @param key
 * @param val
 */
export function setItem(key, val) {
  try {
    localStorage.setItem(key, val + "");
  } catch (e) {
    storage[key] = val + "";
    // sessionStorage.setItem(key, val + "");
  }
}

/**
 * 获取状态
 * @param key
 * @param val
 * @param def
 */
export function getItem(key, def = null) {
  try {
    return localStorage.getItem(key) || def;
  } catch (e) {
    return storage[key] || def;
  }
}

/**
 * 删除状态
 * @param key
 */
export function removeItem(key) {
  try {
    localStorage.removeItem(key);//
  } catch (e) {
    delete storage[key];
  }
}

/**
 * parseInt包装
 * @param val
 * @return {*}
 */
export function parseInt(val) {
  if (val == undefined || val == null) return 0;
  return window.parseInt(val);
}

export function isWeixin() {
  var ua = window.navigator.userAgent.toLowerCase();
  if(ua.match(/MicroMessenger/i) == 'micromessenger'){
    return true;
  }else{
    return false;
  }
}

export function replaceLogin() {
  setItem("nextLocation", location.hash.substring(1, location.hash.indexOf("?")));
  if (isWeixin()) {
    location.replace("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxa5ad24e6ec657969&redirect_uri=https%3A%2F%2Fapp.nagulawyer.xin%2F%23%2FpageLogin&response_type=code&scope=snsapi_base#wechat_redirect");
  } else {
    location.replace("#/pageLogin");
  }
}

/**
 * 关闭微信浏览器
 */
export function closeWindow() {
  if (typeof(WeixinJSBridge)!="undefined") {
    WeixinJSBridge.invoke('closeWindow');
  } else {
    history.back();
  }
}

/**
 * 复制文本
 * @param text
 */
export function copyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.style.position = 'fixed';
  textArea.style.top = 0;
  textArea.style.left = 0;
  textArea.style.width = '2em';
  textArea.style.height = '2em';
  textArea.style.padding = 0;
  textArea.style.border = 'none';
  textArea.style.outline = 'none';
  textArea.style.boxShadow = 'none';
  textArea.style.background = 'transparent';
  textArea.value = text;
  document.body.appendChild(textArea);
  textArea.select();
  let ret = false;
  try {
    ret = document.execCommand('copy');
  } catch (err) {
  }
  document.body.removeChild(textArea);
  return ret;
}

export function getCookie(name) {
  var arr;
  var reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
  if (arr=document.cookie.match(reg)) {
    return unescape(arr[2]);
  }else{
    return null;
  }
}

export function delCookie(name)
{
  let exp = new Date();
  exp.setTime(exp.getTime() - 1);
  let cval= getCookie(name);
  if(cval!=null) document.cookie= name + "="+cval+";expires="+exp.toGMTString();
}

export function delCookieDomain(name, domain)
{
  var exp = new Date();
  exp.setTime(exp.getTime() - 1);
  var cval=getCookie(name);
  if(cval!=null)
    document.cookie= name + "="+cval+ ";domain="+domain+";expires="+exp.toGMTString()+";path=/";
}

export function setCookie(name,value)
{
  let Days = 30;
  let exp = new Date();
  exp.setTime(exp.getTime() + Days*24*60*60*1000);
  document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
}

export function GUID() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    let r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
    return v.toString(16);
  });
}
