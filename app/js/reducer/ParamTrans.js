/**
 * Created by zl on 2017/3/4.
 */
import { PRAMS_TRANS } from '../actions/ActionType';

export default function ParamsTrans(state = {}, action) {
  switch (action.type) {
    case PRAMS_TRANS:
      return action;
    default:
      return state;
  }
}
