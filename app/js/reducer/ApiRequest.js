/**
 * Created by zl on 2017/3/4.
 */
import { GET_DATA_SUCCESS, GET_DATA_FAILED } from '../actions/ActionType';

export default function ApiRequest(state = {code: -2}, action) {
  switch (action.type) {
    case GET_DATA_SUCCESS:
      return Object.assign({}, state, action.data);
    case GET_DATA_FAILED:
      return Object.assign({}, state, {error: action.error});
    default:
      return state;
  }
}
