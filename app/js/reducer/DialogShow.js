/**
 * Created by zl on 2017/3/4.
 */
import { DIALOG_TIP_SHOW, DIALOG_ALERT_SHOW, DIALOG_PROMPT_SHOW } from '../actions/ActionType';

export function TipDialogShow(state = {}, action) {
  if (action.type == DIALOG_TIP_SHOW) {
    return Object.assign({}, state, action);
  }
  return state;
}

export function AlertDialogShow(state = {}, action) {
  if (action.type == DIALOG_ALERT_SHOW) {
    return Object.assign({}, state, action);
  }
  return state;
}

export function PromptDialogShow(state = {}, action) {
  if (action.type == DIALOG_PROMPT_SHOW) {
    return Object.assign({}, state, action);
  }
  return state;
}
