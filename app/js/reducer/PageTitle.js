/**
 * Created by zl on 2017/3/4.
 */
import { SET_TITLE, SET_MENU } from '../actions/ActionType';

export default function PageTitle(state = {title: '404'}, action) {
  switch (action.type) {
    case SET_TITLE:
    {
      document.title = action.title;
      return Object.assign({}, state, {title: action.title});
    }
    case SET_MENU:
      return Object.assign({}, state, {menuTitle: action.title, menuIcon: action.icon,  menuAction: action.action});
    default:
      return state;
  }
}
