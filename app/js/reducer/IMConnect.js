/**
 * Created by zl on 2017/3/4.
 */
import { INIT_IM } from '../actions/ActionType';

export default function IMConnect(state = {count: 0}, action) {
  switch (action.type) {
    case INIT_IM:
      return {count: state.count + 1};
    default:
      return state;
  }
}
