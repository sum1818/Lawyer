/**
 * Created by zl on 2017/3/4.
 */
import { SHOW_TAB } from '../actions/ActionType';

export default function TabsShow(state = {show: true}, action) {
  switch (action.type) {
    case SHOW_TAB:
      return {show: action.show};
    default:
      return state;
  }
}
