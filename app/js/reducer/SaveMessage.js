/**
 * Created by zl on 2017/3/4.
 */
import { ADD_MESSAGE, INSERT_MESSAGE_LIST } from '../actions/ActionType';

export default function SaveMessage(state = {}, action) {
  let data = state[action.targetId];
  if (!data) {
    data = [];
  }
  let newData = {};
  switch (action.type) {
    case ADD_MESSAGE:
      data.push(action.message);
      newData[action.targetId] = data;
      return Object.assign({}, state, newData);
    case INSERT_MESSAGE_LIST:
      if (action.messageList != null && action.messageList.length > 0) {
        action.messageList.forEach(message => {
          data.unshift(message);
        });
      }
      newData[action.targetId] = data;
      return Object.assign({}, state, newData);
    default:
      return state;
  }
}
