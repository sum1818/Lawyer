/**
 * Created by zl on 2017/3/4.
 */
import { SAVE_CACHE,DEL_CACHE } from '../actions/ActionType';

export default function SaveCache(state = {}, action) {
  switch (action.type) {
    case SAVE_CACHE:
      return Object.assign({}, state, action.data);
    case DEL_CACHE:
      let ret = Object.assign({}, state);
      if (action.data) {
        action.data.forEach(item => {
          delete ret[item];
        });
      }
      return ret;
    default:
      return state;
  }
}
