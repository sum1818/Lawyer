/**
 * Created by zl on 2017/3/4.
 */
import { ADD_UNREAD_COUNT } from '../actions/ActionType';

export default function UnRead(state = {count: 0}, action) {
  switch (action.type) {
    case ADD_UNREAD_COUNT:
      return {referId: action.referId, count: state.count + 1, msg: action.msg};
    default:
      return state;
  }
}
