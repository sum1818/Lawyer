/**
 * Created by zl on 2017/3/4.
 */
import { combineReducers } from 'redux';
import TabsShow from './TabsShow';
import PageTitle from './PageTitle';
import ApiRequest from './ApiRequest';
import ParamsTrans from './PageTitle';
import SaveCache from './SaveCache';
import SaveMessage from './SaveMessage';
import IMConnect from './IMConnect';
import UnRead from './UnRead';
import * as DialogShow from './DialogShow';

export const MainReducer =  combineReducers({
  TabsShow,
  PageTitle,
  ApiRequest,
  ParamsTrans,
  SaveCache,
  SaveMessage,
  IMConnect,
  UnRead,
  TipDialogShow:DialogShow.TipDialogShow,
  AlertDialogShow:DialogShow.AlertDialogShow,
  PromptDialogShow:DialogShow.PromptDialogShow
})
