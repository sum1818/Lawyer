/**
 * Created by zl on 2017/3/30.
 */
/**
 * 输入框监听器,控制state
 * @param comId
 * @constructor
 */
export function InputStateChangeHandler(comId, event) {
  if (this.state) {
    let value = event.target.value;
    if (value.indexOf("|") > -1 || value.indexOf(";") > -1 ) {
      value = value.replace(/[|;]/g, "");
    }
    this.state[comId] = value;
    this.setState(this.state);
  }
}

export function InputCallbackChangeHandler(comId, callback, event) {
  if (this.state) {
    let value = event.target.value;
    if (value.indexOf("|") > -1 || value.indexOf(";") > -1 ) {
      value = value.replace(/[|;]/g, "");
    }
    this.state[comId] = value;
    this.setState(this.state);
    if (callback) {
      let newEvent = Object.assign({}, event, {target: {value}});
      callback(newEvent);
    }
  }
}

export function InputSubmitHandleListener(callback, event) {
  if(event.keyCode == "13" && callback) {
    callback();
  }
}
