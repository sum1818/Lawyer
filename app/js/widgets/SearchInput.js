/**
 * Created by zl on 2017/3/30.
 */
import React, { PropTypes } from 'react';
import {Field} from 'amazeui-touch';
import {InputCallbackChangeHandler} from "../handler/InputChangeHandler";

class SearchInput extends React.Component {

  constructor(props) {
    super();
    this.state = {
      value: props.value,
      showHolder: true
    };
  }

  handleClick() {
    this.setState({showHolder:false});
    this.textInput.focus();
  }

  handleBlur() {
    if (!this.state.value) {
      this.setState({showHolder:true});
    }
  }

  handleFocus() {
    this.setState({showHolder:false});
  }

  submitHandleListener(event) {
    if(event.keyCode == "13" && this.props.onSubmit) {
      this.props.onSubmit();
    }
  }

  render() {
    return (
      <div className="bg-light search base-search">
        <div className="content">
          <input
            type="text"
            className="base-search-input field"
            ref={(input) => { this.textInput = input; }}
            onBlur={this.handleBlur.bind(this)}
            onFocus={this.handleFocus.bind(this)}
            onKeyDown={this.submitHandleListener.bind(this)}
            onChange={InputCallbackChangeHandler.bind(this, "value", this.props.onChange)}
            value={this.state.value}
          />
          <div className={`base-search-holder ${this.state.showHolder? "":"base-hide"}`} onClick={this.handleClick.bind(this)}>
            <i className="base-icon-search-gray"/>
            {this.props.placeholder}
          </div>
        </div>
      </div>
    );
  }
}

SearchInput.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  placeholder:PropTypes.string.isRequired
};

SearchInput.defaultProps = {
  value: ""
};

export default SearchInput;
