/**
 * Created by zl on 2017/3/30.
 */
import React, { PropTypes } from 'react';
import {NavBar} from 'amazeui-touch';
import {InputCallbackChangeHandler} from "../handler/InputChangeHandler";

class SearchNav extends React.Component {

  constructor(props) {
    super();
    this.state = {
      value: props.value
    };
  }

  submitHandleListener(event) {
    if(event.keyCode == "13" && this.props.onSubmit) {
      this.props.onSubmit();
    }
  }

  handleClick(comId) {
    switch (comId) {
      case 'clear_btn':
        this.setState({value:""});
        break;
    }
  }

  render() {
    return (
      <NavBar
        amStyle="dark"
      >
        <div className="flex-row match-parent-width search-nav">
          <div className="flex-1 padding-xs">
            <div className="flex-row flex-v-center input">
              <i className="base-icon-search-gray middle-icon margin-left-xs"/>
              <input type="search"
                     ref={(input) => { this.textInput = input; }}
                     className="margin-0 padding-xs sm-size"
                     value={this.state.value}
                     onKeyDown={this.submitHandleListener.bind(this)}
                     onChange={InputCallbackChangeHandler.bind(this, "value", this.props.onChange)}
                     placeholder={this.props.placeholder}/>
              <i onClick={this.handleClick.bind(this, 'clear_btn')} className={`base-icon-clear middle-icon margin-right-xs ${this.state.value?"":"base-invisible"}`}/>
            </div>
          </div>
          <div onClick={this.props.onCancel} className="navbar-nav-item padding-left-sm">取消</div>
        </div>
      </NavBar>
    );
  }
}

SearchNav.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  placeholder:PropTypes.string.isRequired
};

SearchNav.defaultProps = {
  value: ""
};

export default SearchNav;
