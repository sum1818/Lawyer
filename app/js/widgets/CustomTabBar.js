/**
 * Created by zl on 2017/3/28.
 */
import {
  TabBar,
} from 'amazeui-touch';
import React from 'react';
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
import _classnames from 'classnames';
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
var _classnames2 = _interopRequireDefault(_classnames);
function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

class CustomTabBar extends TabBar {
  render() {
    var classSet = this.getClassSet();
    var _props = this.props;
    var Component = _props.component;
    var className = _props.className;
    var children = _props.children;
    var onAction = _props.onAction;
    var props = _objectWithoutProperties(_props, ['component', 'className', 'children', 'onAction']);
    delete props.classPrefix;
    delete props.amStyle;

    return React.createElement(Component, _extends({}, props, {
      className: (0, _classnames2.default)(classSet, className)
    }), children.map((child, index) => {
      var _child$props = child.props;
      var eventKey = _child$props.eventKey;
      var onClick = _child$props.onClick;
      var props = _objectWithoutProperties(_child$props, ['eventKey', 'onClick']);
      var clickHandler = onClick || onAction;
      var key = eventKey || index;
      eventKey = eventKey || key;
      return React.createElement(CustomTabBarItem, Object.assign({}, props, {
        onClick: clickHandler.bind(null, eventKey),
        key: key,
        eventKey: eventKey
      }));
    }));
  }
}

class CustomTabBarItem extends TabBar.Item {
  renderIcon2() {
    var icon = this.props.icon;
    return icon ? React.createElement(
        "div",
        { className: `tabbar-item-container base-icon-${icon}`, key: icon + "_ico"},
        this.renderBadge()
      ) : null;
  }
  render() {
    var classSet = this.getClassSet(true);
    var _props3 = this.props;
    var Component = _props3.component;
    var className = _props3.className;

    var props = _objectWithoutProperties(_props3, ['component', 'className']);

    delete props.classPrefix;
    delete props.badge;
    delete props.badgeStyle;
    delete props.eventKey;
    delete props.onAction;

    Component = this.props.href ? 'a' : Component;
    return React.createElement(
      Component,
      _extends({}, props, {
        className: (0, _classnames2.default)(classSet, className, this.prefixClass('item'))
      }),
      [this.renderIcon2(), this.renderTitle()]
    );
  }
}

export default CustomTabBar;
