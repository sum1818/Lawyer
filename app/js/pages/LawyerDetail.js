import PageBase from './PageBase';
import React from "react";
import {
  Container,
  Group,
  List,
  Button
} from 'amazeui-touch';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle, setMenu, saveCache} from "../actions/ActionCreator";
import * as publicApi from "../apis/PublicApi";
import {ASSETS_HOST} from '../const';

class LawyerDetail extends PageBase {

  static contextTypes = {//注入router对象
    router: React.PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      lawyerInfo:{}
    };
  }

  componentWillMount() {
    this.props.setTitle("律师资料");
    this.props.setMenu(null, null, null);
    let lawyerId;
    if (this.props.location.action == "PUSH") {
      lawyerId = this.props.location.state.id;
      this.props.saveCache({detailLawyerId:lawyerId});
    } else {
      lawyerId = this.props.detailLawyerId;
    }
    publicApi.lawyerDetail(lawyerId)
      .then(data => {
        if (data.id) {
          this.setState({lawyerInfo: data});
        }
      })
  }

  handleClick() {
    this.context.router.push({pathname:'/messageDetail', state: {referId:this.props.detailLawyerId, utype:1,name: this.state.lawyerInfo.name}});
  }

  render() {
    const bgStyle = {
      background: `url(${require("../../image/my_bg.png")}) 0 0 / 100% 100%`
    };
    const lawyer = this.state.lawyerInfo;
    return (
      <Container className="flex-column">
        <div className="base-background text-center" style={bgStyle}>
          <img src={lawyer.head?(ASSETS_HOST+lawyer.head):require("../../image/lawyer_head.png")} className="head-pic"/>
          <div className="ms-size white-color padding-bottom-sm">{lawyer.name||""}</div>
        </div>
        <div className="flex-row sm-size padding-h-sm padding-top-sm white-background">
          <div className="flex-1 gray-color"><span className="middle-color">执业: </span>{lawyer.years}年</div>
          <div className="flex-1 gray-color"><span className="middle-color">地域: </span>{lawyer.area}</div>
        </div>
        <div className="sm-size gray-color white-background padding-h-sm padding-top-xs padding-bottom-sm"><span className="middle-color">专长: </span>{lawyer.specialty}</div>
        <div className="white-background padding-sm margin-top-xs flex-1 scroll-div ms-size">
          简介
          <div className="gray-color margin-top-xs base-line-height">
            {lawyer.synopsis}
          </div>
        </div>
        <div className="flex-row ms-size base-border-up">
          <div className="flex-2 padding-sm main-color white-background">提供简要咨询服务</div>
          <div className="flex-1 text-center padding-sm white-color main-background" onClick={this.handleClick.bind(this)}>咨询TA</div>
        </div>
      </Container>
    );
  }
}

export default connect(state => state.SaveCache, dispatch => bindActionCreators({setTitle,setMenu,saveCache}, dispatch))(LawyerDetail);
