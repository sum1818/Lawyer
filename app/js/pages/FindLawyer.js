import PageBase from './PageBase';
import React from "react";
import {Container, Group, List} from "amazeui-touch";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle, setMenu} from "../actions/ActionCreator";
import * as publicApi from "../apis/PublicApi";
import {ASSETS_HOST} from '../const';

class FindLawyer extends PageBase {

  static contextTypes = {//注入router对象
    router: React.PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.state = {
      lawyers: []
    }
  }

  componentWillMount() {
    this.props.setTitle("找律师");
    this.props.setMenu(null, "search-white", () => {
      this.context.router.push({pathname:'/searchLawyer'});
    });
    publicApi.queryLawyers()
      .then(data => {
        if (data.code == 0 && data.list) {
          this.setState({lawyers: data.list});
        }
      });
  }

  handleClick(comId, item, event) {
    switch (comId) {
      case 'lawyer-detail':
        this.context.router.push({pathname:'/lawyerDetail',state: {id: item.id}});
        break;
      case 'lawyer-chat':
        this.context.router.push({pathname:'/messageDetail', state: {referId:item.id, utype:1,name: item.name}});
        break;
    }
    event.stopPropagation();
  }

  render() {
    return (
      <Container scrollable>
        <List className="margin-0 index_lawyer_list base-background">
          {
            this.state.lawyers.map((lawyer,i) => {
              return(
                <List.Item key={"lawyer" + i} onClick={this.handleClick.bind(this, 'lawyer-detail', lawyer)} className="padding-0 margin-h-xsm margin-top-xsm white-background">
                  <div className="item-main padding-h-xs padding-v-sm">
                    <div><img src={lawyer.head?(ASSETS_HOST+lawyer.head):require("../../image/news.png")}/></div>
                    <div className="block-div content">
                      <div className="item-main">
                        <div className="title flex-1">{lawyer.name}</div>
                        <div className="consult-btn" onClick={this.handleClick.bind(this, 'lawyer-chat',lawyer)}>咨询TA</div>
                      </div>
                      <div><span>执业:</span> {lawyer.years}年</div>
                      <div><span>地域: </span>{lawyer.area}</div>
                      <div><span>专长: </span>{lawyer.specialty}</div>
                    </div>
                  </div>
                </List.Item>
              )
            })
          }
        </List>
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu}, dispatch))(FindLawyer);
