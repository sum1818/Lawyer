/**
 * Created by zl on 2017/11/27.
 */
import PageBase from "./PageBase";
import React from "react";
import {Container, Group, List, Icon, Button} from "amazeui-touch";
import {getOrder} from '../apis/UserApi'
import {convertDate} from '../utils/XUtil';

export default class BuyResult extends PageBase {

  componentWillMount() {
    document.title = "服务卡购买成功";
    this.state = {orderNo: this.props.location.query.orderNo, order: {}};
    this.state.orderNo && getOrder(this.state.orderNo).then(result => {
      if (result) {
        this.setState({order: result});
      }
    });
  }

  handleClick() {
    location.replace("#/product");
  }

  render() {
    return (
      <Container fill>
        <Group component="span">
          <Icon name="check" className="green-color"/>
          恭喜您购买服务成功！
        </Group>
        <List className="margin-top-xs sm-size">
          <List.Item title="订单号："  after={this.state.orderNo}/>
          <List.Item title="下单时间："  after={this.state.order.createtime?convertDate(new Date(this.state.order.createtime*1000), 'YYYY-MM-DD hh:mm:ss'):""}/>
          <List.Item title="付款金额："  after={this.state.order.money?this.state.order.money + '元':''}/>
        </List>
        <div className="margin-h-sm">
          <Button amStyle="success" onClick={this.handleClick.bind(this)} block>返回服务</Button>
        </div>
      </Container>
    );
  }
}
