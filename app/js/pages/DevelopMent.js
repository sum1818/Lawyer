import PageBase from './PageBase';
import React from "react";
import {
  Container,
  Group,
  Loader
} from 'amazeui-touch';

export default  class DevelopMent extends PageBase {

  componentWillMount() {
    document.title = '服务';
  }
  render() {
    return (
      <Container>
        <div className="lg-size base-center margin-top-xsm">
          <Loader amStyle="secondary" rounded="rounded"/>
        </div>
        <div className="lg-size base-center margin-top-xsm">
          正在开发，敬请期待...
        </div>
      </Container>
    );
  }
}
