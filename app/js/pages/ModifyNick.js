import PageBase from "./PageBase";
import React from "react";
import {Container, Group, Field} from "amazeui-touch";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle, setMenu} from "../actions/ActionCreator";
import * as userApi from "../apis/UserApi";
import showToast from "../utils/ToastUtil";

class ModifyNick extends PageBase {

  constructor() {
    super();
  }

  componentWillMount() {
    this.state = this.props.location.state;
    this.props.setTitle("修改昵称");
    this.props.setMenu("保存", null, () => {
      let name = this.state['name_input'];
      if (name == null || name.trim().length == 0) {
        showToast("昵称不能为空");
        return;
      }
      userApi.modifyName(name)
        .then(data => {
          if (data.code == 0) {//修改名称成功
            showToast("修改昵称成功");
            history.back();
          } else {
            showToast(data.error);
          }
        });
    });
  }

  componentWillUnmount() {
    this.props.setMenu(null, null);
    super.componentWillUnmount();
  }

  render() {
    return (
      <Container>
        <Group component="span">
          <Field
            label="请输入新的昵称"
            onChange={this.defaultHandleChange.bind(this, "name_input")}
            defaultValue={this.state['name_input']}
          />
        </Group>
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu}, dispatch))(ModifyNick);
