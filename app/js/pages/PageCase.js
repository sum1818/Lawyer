import PageBase from './PageBase';
import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle, setMenu} from "../actions/ActionCreator";
import {
  Container,
  Group,
  Tabs,
  List,
  Field,
  Icon,
  View,
  NavBar,
  Button,
  Card,
  ButtonGroup
} from 'amazeui-touch';
import {Link} from "react-router";
import SearchInput from "../widgets/SearchInput";
import * as lawyerApi from "../apis/LawyerApi";
import * as XUtil from "../utils/XUtil";
import {partyTypes,caseStates,sessionTypes} from "../const";

class PageCase extends PageBase {

  static contextTypes = {//注入router对象
    router: React.PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.state = {
      activeTab:0,
      waitCases:[],
      finishedCases:[]
    }
  }

  handleAction(key) {
    this.setState({
      activeTab: key
    });
    if (this.state.finishedCases.length == 0 && key == 1) {
      lawyerApi.getCaseList(true, -1)
        .then(data=> {
          if (data.code == 0 && data.list != null) {
            this.setState({finishedCases:data.list});
          }
        });
    }
  }

  handleClick(comId, caseId) {
    switch (comId) {
      case 'detail_div':
        XUtil.removeItem("editCase");//清空之前的缓存
        XUtil.removeItem("editLoad");//清空之前的缓存
        this.context.router.push({pathname:'/caseDetail',state: {id: caseId}});
        break;
    }
  }

  handleEvent(item, event) {
    event.preventDefault();
    switch (item.type) {
      case "back":
        history.back();
        break;
      case "add":
        XUtil.removeItem("editCase");//清空之前的缓存
        location.href = "#/addCase";
        break;
    }
  }

  searchHandle() {
    if (this.state['search_input']) {
      this.context.router.push({pathname:'/searchCase',state: {name:this.state['search_input']}});
    } else {
      showToast("请输入案号或案由");
    }
  }

  componentWillMount() {
    document.title = "案件管理";
    if (this.state.waitCases.length == 0) {
      lawyerApi.getCaseList(false, -2)
        .then(data=> {
          if (data.code == 0 && data.list != null) {
            this.setState({waitCases:data.list});
          }
        });
    }
  }

  handleScroll(event) {
    let wholeHeight=event.currentTarget.scrollHeight;
    let scrollTop=event.currentTarget.scrollTop;
    let divHeight=event.currentTarget.clientHeight;
    if(divHeight+scrollTop>=wholeHeight){//加载更多数据
      if (this.state.activeTab == 0) {
        let lastId = this.state.waitCases.length > 0 ? this.state.waitCases[this.state.waitCases.length - 1].id:-2;
        lawyerApi.getCaseList(false, lastId)
          .then(data=> {
            if (data.code == 0 && data.list != null && data.list.length > 0) {
              let waitCases = this.state.waitCases;
              waitCases.extend(data.list);
              this.setState({waitCases});
            }
          });
      } else {
        let lastId = this.state.finishedCases.length > 0 ? this.state.finishedCases[this.state.finishedCases.length - 1].id:-2;
        lawyerApi.getCaseList(true, lastId)
          .then(data=> {
            if (data.code == 0 && data.list != null && data.list.length > 0) {
              let finishedCases = this.state.finishedCases;
              finishedCases.extend(data.list);
              this.setState({finishedCases});
            }
          });
      }
    }
  }

  createCaseList(cases) {
    return cases.map((dealCase, i) => {
      let delegates = dealCase.delegates ? dealCase.delegates.split(";"):[];
      delegates = delegates.map((delegate, j) => {//计算代理人
        let arr = delegate.split("|");
        let name = arr.length > 0 ? arr[0]: "";
        let type = arr.length > 2 ? partyTypes[parseInt(arr[2])] : "";
        return (
          <div key={"delegate" + j}  className="item-main base-border-down padding-h padding-v-sm"><div>{type}</div><div>{name}</div></div>
        );
      });
      let sessions = dealCase.sessions ?  dealCase.sessions.split(";"):[];
      let sessionTime = "", sessionPlace = "", sessionType = "";
      for (let j in sessions) {
        if (sessions.hasOwnProperty(j)) {
          let arr = sessions[j].split("|");
          let state = arr.length > 1 ? parseInt(arr[1]) : 0;
          let name = arr.length > 0 ? arr[0] : "";
          let time = arr.length > 2 ? XUtil.convertDate(new Date(parseInt(arr[2])), "YYYY-M-D hh:mm") : "";
          let place = arr.length > 3 ? arr[3] : "";
          if (state == dealCase.state) {
            sessionTime = <div key={"sessionTime" + j}  className="item-main base-border-down padding-h padding-v-sm"><div className="flex-4">{name}开庭时间</div><div className="flex-6 text-right">{time}</div></div>;
            sessionPlace = <div key={"sessionPlace" + j}  className="item-main base-border-down padding-h padding-v-sm"><div className="flex-4">{name}开庭地点</div><div className="flex-6 text-right">{place}</div></div>;
            break;
          }
        }
      }
      return (
        <List.Item key={"key" + dealCase.id} className="white-background margin-bottom-xs padding-0">
          <div className="block-div match-parent-width ms-size">
            <div className="item-main base-border-down padding-h padding-v-sm"><div>案由</div><div>{dealCase.reason}</div></div>
            {delegates}
            {sessionTime}
            {sessionPlace}
            <div className="item-main padding-h padding-v-sm sm-size dark-color" onClick={this.handleClick.bind(this, "detail_div", dealCase.id)}>
              <div className="block-div">
                <div>案号 : {dealCase.casenum}</div>
                <div>当前阶段 : <span className="main-color">{caseStates[dealCase.state]}</span></div>
                <div>创建时间 : {XUtil.convertDate(new Date(dealCase.createtime*1000),"YYYY-M-D")}</div>
              </div>
              <div className="main-color">案件详情<i className="base-icon-right-gray right-arrow"/></div>
            </div>
          </div>
        </List.Item>
      );
    });
  }

  render() {
    const titleTab = (
      <ButtonGroup amSize="xxs" className="margin-v-0" hollow>
        <Button className={`btn-left-radius btn-white ${this.state.activeTab == 0 ? "hover" : ""}`} onClick={this.handleAction.bind(this, 0)}>案件</Button>
        <Button className={`btn-right-radius btn-white ${this.state.activeTab == 1 ? "hover" : ""}`} onClick={this.handleAction.bind(this, 1)}>已结案</Button>
      </ButtonGroup>
    );

    let navTitle = {
      title: titleTab,
      leftNav: [{component: Link, className:"base-icon-back navbar-icon", type: 'back'}],
      rightNav:[{component: Link, className:"base-icon-plus-white navbar-icon", type: 'add'}],
      onAction: this.handleEvent.bind(this)
    };

    return (
      <View>
        <NavBar {...navTitle}
                amStyle="dark"
        />
        <Container className="match-parent-height-vertical">
          <SearchInput
            placeholder="搜索案件"
            onSubmit={this.searchHandle.bind(this)}
            onChange={this.defaultHandleChange.bind(this, 'search_input')}
          />
          <Tabs
            className="tab-hide margin-0 scroll-tabs"
            activeKey={this.state.activeTab}
          >
            <Tabs.Item
              className="padding-0"
              onScroll={this.handleScroll.bind(this)}
              key={0}
            >
              <div>
                <List className="margin-0 base-background">
                  {
                    this.createCaseList(this.state.waitCases)
                  }
                </List>
              </div>
            </Tabs.Item>
            <Tabs.Item
              className="padding-0"
              onScroll={this.handleScroll.bind(this)}
              key={1}
            >
              <List className="margin-0 base-background">
                {
                  this.createCaseList(this.state.finishedCases)
                }
              </List>
            </Tabs.Item>
          </Tabs>
        </Container>
      </View>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle, setMenu}, dispatch))(PageCase);
