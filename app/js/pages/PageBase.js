/**
 * Created by zl on 2017/3/5.
 */
import React from 'react';
import * as XUtil from "../utils/XUtil";

export default class PageBase extends React.Component {

  /**
   * 默认onChange事件
   * @param comId
   * @param event
   */
  defaultHandleChange(comId, event) {
    if (this.state) {
      let value = event.target.value;
      if (value.indexOf("|") > -1 || value.indexOf(";") > -1 ) {
        value = value.replace(/[|;]/g, "");
      }
      this.state[comId] = value;
      this.setState(this.state);
    }
  }

  componentWillUnmount() {
    if (this.props.location) {
      sessionStorage.removeItem(`@@History/${this.props.location.key}`)
    }
  }

}
