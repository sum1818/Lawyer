import PageBase from "./PageBase";
import React from "react";
import {Container, Group, List, Field, Icon, Button, Modal} from "amazeui-touch";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle, setMenu, initIM} from "../actions/ActionCreator";
import * as XUtil from "../utils/XUtil";
import * as userApi from "../apis/UserApi";
import * as messageApi from "../apis/MessageApi";
import showToast from "../utils/ToastUtil";

class PageLogin extends PageBase {

  static contextTypes = {//注入router对象
    router: React.PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      remain: 60,
      sendDisabled: false,
      codeTip:'获取验证码'
    };
  }

  componentWillMount() {
    // this.props.setTitle("登录/注册");
    // this.props.setMenu(null, null, null);
    this.props.initIM();
  }

  componentWillUnmount() {
    clearInterval(this.state.left);
    super.componentWillUnmount();
  }

  descCounter() {
    this.state.remain --;
    if (this.state.remain <= 0) {
      clearInterval(this.state.left);
      this.setState({codeTip:"发送验证码", sendDisabled:false});
    } else {
      this.setState({codeTip:`${this.state.remain}秒后重新发送`, sendDisabled:true});
    }
  }

  handleClick(comId) {
    switch (comId) {
      case 'send_btn':
        if (XUtil.isMobile(this.state['mobile_input'])) {
          userApi.sendLoginCode(this.state['mobile_input'])
            .then(data => {
              if (data.code == 0) {
                //开始60秒倒计时
                clearInterval(this.state.left);
                this.state.remain = 60;
                this.state.left = setInterval(this.descCounter.bind(this), 1000);
                showToast(data.result);
              } else {
                showToast(data.error);
              }
            });
        } else {
          showToast("请输入正确的手机号");
        }
        break;
      case 'login_btn':
        if (!XUtil.isMobile(this.state['mobile_input'])) {
          showToast("请输入正确的手机号");
        } else if (!this.state['pass_input']) {
          showToast("验证码或密码不能为空");
        } else {
          userApi.sendLogin(this.state['mobile_input'], this.state['pass_input']).then(data => {
            let nextLocation = XUtil.getItem("nextLocation");
            if (nextLocation) {
              this.context.router.replace({ pathname: nextLocation });
              XUtil.removeItem("nextLocation");
            } else {
              history.back();
            }
          }).catch(err => {
            showToast(err.message);
          });
        }
        break;
    }
  }

  render() {
    return (
      <Container fill direction="column">
        <Field
          type="number"
          labelBefore={<i className="base-icon-mobile" />}
          containerClassName="no-arrow login-field"
          placeholder="请输入您的手机号"
          onChange={this.defaultHandleChange.bind(this, "mobile_input")}
          defaultValue={this.state['mobile_input']}
        />
        <Field
          type="password"
          labelBefore={<i className="base-icon-auth" />}
          btnAfter={<Button id="send_btn" className="btn-send" disabled={this.state.sendDisabled} onClick={this.handleClick.bind(this, "send_btn")}>{this.state.codeTip}</Button>}
          containerClassName="no-arrow login-field"
          placeholder="输入验证码或登录密码"
          defaultValue={this.state['pass_input']}
          onChange={this.defaultHandleChange.bind(this, "pass_input")}
        />
        <div className="margin-top-lg padding-h-sm">
          <Button amStyle="primary" onClick={this.handleClick.bind(this, "login_btn")} block>登录</Button>
        </div>
        <Container fill>
        </Container>
        <div
          className="margin-v base-center">
          <a href="#/protocol">用户注册与使用协议</a>
        </div>
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu,initIM}, dispatch))(PageLogin);
