import PageBase from './PageBase';
import React from "react";
import {Container, Group, Field, List, Button} from "amazeui-touch";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle,setMenu} from "../actions/ActionCreator";
import * as lawyerApi from "../apis/LawyerApi";
import * as XUtil from "../utils/XUtil";
import showToast from "../utils/ToastUtil";

class AddCustomer extends PageBase {

  constructor() {
    super();
    this.state = {

    }
  }

  componentWillMount() {
    this.props.setTitle("添加客户");
  }

  handleClick(comId) {
    switch (comId) {
      case 'add_btn':
        if (!XUtil.isMobile(this.state['mobile_input'])) {
          showToast("手机号码格式不正确");
          return;
        }
        lawyerApi.addGuest(this.state['mobile_input'], this.state['name_input'])
          .then(data => {
            if (data.code == 0) {
              showToast(data.result);
              history.back();
            } else {
              showToast(data.error);
            }
          });
        break;
    }
  }

  render() {
    return (
      <Container>
        <List className="list-small-size margin-top-sm">
          <List.Item nested="input" title="客户姓名:">
            <Field
              className="flex-3"
              placeholder="请输入客户姓名"
              onChange={this.defaultHandleChange.bind(this, 'name_input')}
            />
          </List.Item>
          <List.Item nested="input" title="联系电话:">
            <Field
              className="flex-3"
              containerClassName="no-arrow"
              placeholder="请输入客户手机号码"
              type="number"
              onChange={this.defaultHandleChange.bind(this, 'mobile_input')}
            />
          </List.Item>
        </List>
        <div className="base-padding">
          <Button id="add_btn" amStyle="primary" onClick={this.handleClick.bind(this, "add_btn")} block>确定添加</Button>
        </div>
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu}, dispatch))(AddCustomer);
