import PageBase from "./PageBase";
import React from "react";
import {Container, Group, List, Field, Button} from "amazeui-touch";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle, setMenu, showPrompt,saveCache} from "../actions/ActionCreator";
import * as userApi from "../apis/UserApi";
import PromptDialog from "../dialogs/PromptDialog";
import * as lawyerApi from "../apis/LawyerApi";
import showToast from "../utils/ToastUtil";
import {ASSETS_HOST} from '../const';

class CustomerDetail extends PageBase {

  static contextTypes = {//注入router对象
    router: React.PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {customerId:0};
  }

  componentWillMount() {
    this.props.setTitle("详细资料");
    this.props.setMenu(null, null, null);
    if (this.props.location.action == "PUSH") {
      this.state.customerId = this.props.location.state.id;
      this.props.saveCache({detailCustomerId:this.state.customerId});
    } else {
      this.state.customerId = this.props.detailCustomerId;
    }
    userApi.getReferInfo(this.state.customerId)
      .then(data => {
        if (data.code == 0) {
          data.valid = 0;
          this.setState(data);
        } else {
          this.setState({valid:1});
        }
      });
  }

  handleClick(comId) {
    switch (comId) {
      case 'add_btn':
        this.state['remark_input'] = null;
        this.props.showPrompt("提示", data => {
          if (data != null) {
            let remark = data.trim();
            if (remark.length == 0) {
              showToast("请输入客户备注名");
              return false;
            }
            lawyerApi.addGuest(this.state.mobile, remark)
              .then(result => {
                if (result.code == 0) {
                  showToast(result.result);
                  this.setState({valid: 0, remark});
                } else {
                  showToast(result.error);
                }
              });
          }
          return true;
        });
        break;
      case 'edit_btn':
        !this.state.valid && this.props.showPrompt("提示", data => {
          if (data != null) {
            let remark = data.trim();
            if (remark.length == 0) {
              showToast("请输入客户备注名");
              return false;
            }
            userApi.modifyRemark(this.state.id, remark)
              .then(data => {
                if (data.code == 0) {//修改名称成功
                  showToast(data.result);
                  this.setState({remark});
                } else {
                  showToast(data.error);
                }
              });
          }
          return true;
        });
        break;
      case 'customer-chat':
        this.context.router.push({pathname:'/messageDetail', state: {referId:this.state.customerId, utype:0 ,name:this.state.remark||this.state.name}});
        break;
    }
  }

  render() {
    const bgStyle = {
      background: `url(${require("../../image/my_bg.png")}) 0 0 / 100% 100%`
    }
    return (
      <Container>
        <div className="base-background text-center" style={bgStyle}>
          <img src={this.state.head?(ASSETS_HOST+this.state.head):require("../../image/user_head.png")} className="head-pic"/>
          <div className="ms-size white-color padding-bottom-sm">{this.state.name||""}</div>
        </div>
        <List className="margin-0 list-small-size">
          <List.Item title="手机号" after={this.state.mobile} />
          <List.Item title="备注" onClick={this.handleClick.bind(this,'edit_btn')} after={this.state.remark} />
          <List.Item href="#/pageCase" title="关联案件" className="base-hide"/>
        </List>
        <div className="margin-top-lg padding-h-sm">
          {
            this.state.valid===0?(
                <Button amStyle="primary" onClick={this.handleClick.bind(this, 'customer-chat')} block>发消息</Button>
              ):(
                <Button amStyle="primary" onClick={this.handleClick.bind(this, 'add_btn')} block>添加客户</Button>
              )
          }
        </div>
        <PromptDialog
          title="提示"
          isOpen={false}>
          <Field placeholder="请输入客户备注名" onChange={this.defaultHandleChange.bind(this,'remark_input')}/>
        </PromptDialog>
      </Container>
    );
  }
}

export default connect(state => state.SaveCache, dispatch => bindActionCreators({setTitle,setMenu,showPrompt,saveCache}, dispatch))(CustomerDetail);
