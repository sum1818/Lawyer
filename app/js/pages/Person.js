import React from "react";
import {Container, Group, NavBar, List, Badge, View, Button} from "amazeui-touch";
import {Link} from "react-router";
import * as userApi from "../apis/UserApi";
import {ASSETS_HOST} from '../const';
import * as XUtil from "../utils/XUtil";

export default class Person extends React.Component {

  static contextTypes = {//注入router对象
    router: React.PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.state = {
      userInfo: {

      }
    }
  }

  componentWillMount() {
    document.title = "用户中心";
    userApi.getUserInfo()
      .then(data => {
        if (data.code == 0) {//已经登录
          this.setState({userInfo:data});
        } else {//没有登录
          this.setState({userInfo:{}});
        }
      });
  }

  handleClick() {
    let nextLocation = this.state.userInfo.id?"/pageSetting":"/pageLogin"
    if (!this.state.userInfo.id) {
      XUtil.setItem("nextLocation", this.props.location.pathname);
    }
    this.context.router.replace({ pathname: nextLocation });
  }

  render() {
    const rightNav = {
      className:"base-icon-setting navbar-icon",
      href:this.state.userInfo.id?"#/pageSetting":"#/pageLogin"
    };
    const bgStyle = {
      background: `url(${require("../../image/my_bg.png")}) 0 0 / 100% 100%`
    };
    return (
      <View>
        <div className="text-center" style={bgStyle}>
          <NavBar
            amStyle="primary"
            rightNav={[rightNav]}
          />
          <a onClick={this.handleClick.bind(this)}><img src={this.state.userInfo.head?(ASSETS_HOST+this.state.userInfo.head):require("../../image/user_head.png")} className="head-pic"/></a>
          <div className="ms-size white-color margin-bottom">{this.state.userInfo.id ? this.state.userInfo.name||" " : "点击头像登录"}</div>
        </div>
        <Container className="margin-0" scrollable>
          <List className={`margin-top-0 margin-bottom-sm my-list base-hide`}>
            <List.Item href="#/pageCustomer" media={<i className="base-icon-order"/>} title="咨询订单" />
          </List>
          <List className={`margin-top-0 margin-bottom-sm my-list ${String(this.state.userInfo.type).charAt(0) == '1'? "base-show":"base-hide"}`}>
            <List.Item href="#/pageCustomer" media={<i className="base-icon-customer"/>} title="客户管理" />
            <List.Item href="#/addCustomer" media={<i className="base-icon-customer-gray"/>} title="添加客户" />
            <List.Item href="#/pageCase" media={<i className="base-icon-case"/>} title="案件管理" />
            <List.Item href="#/pageLawyer" media={<i className="base-icon-lawyer"/>}  className={String(this.state.userInfo.type).substring(0,3) == '100'? "base-show":"base-hide"} title="律师管理" />
          </List>
          <List className="margin-top-0 margin-bottom-sm my-list">
            <List.Item href="#/pageCustomer" media={<i className="base-icon-feedback"/>} title="意见反馈" className="base-hide"/>
            <List.Item href="#/about" media={<i className="base-icon-about"/>} title="关于我们" />
          </List>
        </Container>
      </View>
    );
  }
}
