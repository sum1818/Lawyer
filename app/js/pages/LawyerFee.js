/**
 * Created by zl on 2017/11/10.
 */
import PageBase from './PageBase';
import React from "react";
import {
  Container,
  Group,
  Field,
  Button
} from 'amazeui-touch';
import * as XUtil from '../utils/XUtil'
import showToast from '../utils/ToastUtil'

const generalFee = ['2500~10000元', '1.侦查阶段：1500-8000元/件。2.审查起诉阶段：1500-10000元/件。3.一审审判阶段：2500-25000元/件。4.代理刑事自诉案件或者担任被害人代理人的，可按照上述标准酌减收费。'];

const extendFee = {
  min: 2500,
  ext: ['100000-0.06-0.08', '500000-0.05-0.06', '1000000-0.04-0.05', '5000000-0.03-0.04', '10000000-0.02-0.03', '-0.01-0.02']
};

export default class LawyerFee extends PageBase {

  componentWillMount() {
      document.title = '律师费计算';
      this.state = {type: 0, property: 0, money: '', show: false, result: ''};
  }

  handleChange(comId, event) {
    if (comId == 'type') {
      let type = event.target.value;
      this.setState({type});
    } else if (comId == 'property') {
      let property = event.target.value;
      this.setState({property});
    } else {
      let money = event.target.value;
      this.setState({money});
    }
  }

  handleClick() {
    if (this.state.property == 0) {
      if (this.state.money.length > 15) {
        showToast('标的金额最多只能保留15位数字');
        return;
      } else if (!XUtil.isNumber(this.state.money)) {
        showToast('标的金额必须为整数');
        return;
      }
    }
    let result = '';
    if (this.state.property != 0) {
      result = generalFee[this.state.type];
    } else if (this.state.type == 0) {
      let money = parseInt(this.state.money);
      let minCostMoney = 0, maxCostMoney = 0;
      let lastLimit = 0;
      for (let item of extendFee.ext) {//
        let limits = item.split('-');
        let limitMoney = parseInt(limits[0]);
        let minRate = parseFloat(limits[1]);
        let maxRate = parseFloat(limits[2]);
        if (money > limitMoney) {
          minCostMoney += (limitMoney - lastLimit) * minRate;
          maxCostMoney += (limitMoney - lastLimit) * maxRate;
        } else {
          minCostMoney += (money - lastLimit) * minRate;
          maxCostMoney += (money - lastLimit) * maxRate;
          break;
        }
        lastLimit = limitMoney;
      }
      minCostMoney = minCostMoney < extendFee.min ? extendFee.min : minCostMoney;
      maxCostMoney = maxCostMoney < extendFee.min ? extendFee.min : maxCostMoney;
      if (maxCostMoney <= extendFee.min) {
        result = `${maxCostMoney.toFixed(3)}元`;
      } else {
        result = `${minCostMoney.toFixed(3)}~${maxCostMoney.toFixed(3)}元`;
      }
    } else {
      result = '实行政府指导价';
    }
    this.setState({show: true, result});
  }

  render() {
    let checkForm = this.state.property != 0 || XUtil.isNumber(this.state.money);
    return (
      <Container scrollable className="white-background">
        <Group className="margin-0">
          <label>案件类型：民事（涉及财产关系）</label>
          {
            this.state.property == 0 ? (
                <Field
                  label="标的金额："
                  placeholder="请输入标的金额（单位:元)"
                  type="number"
                  maxLength="15"
                  onChange={this.handleChange.bind(this, 'money')}
                  defaultValue={this.state.money}
                />
              ):null
          }
        </Group>
        {
          this.state.show? (
          <Group className="margin-v-0">
          计算结果：{this.state.result}
          </Group>
          ):null
        }
        <Group className="margin-v-0">
          说明：上述律师费的计算依据【浙价服（2015）203号《浙江省物价局 浙江省司法厅关于完善律师和基层法律服务收费的通知》】，但该计算结果并非最终的律师费金额。律师费的计算还会结合案件所在地区、办案律师资历、案件所属类型、整体难易程度等众多因素进行市场调节。
        </Group>
        <Group
          className="margin-0"
          component="span">
          <Button amStyle="success" block disabled={!checkForm} onClick={this.handleClick.bind(this,'submit')}>开始计算</Button>
        </Group>
      </Container>
    );
  }
}
