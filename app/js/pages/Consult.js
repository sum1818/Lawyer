/**
 * Created by zl on 2017/10/11.
 */
import PageBase from './PageBase';
import React from "react";
import {
  Container,
  Group,
  Button,
  List,
  Field,
  Icon
} from 'amazeui-touch';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle} from "../actions/ActionCreator";
import {addConsult} from "../apis/UserApi";
import * as XUtil from "../utils/XUtil";
import showToast from "../utils/ToastUtil";

class Consult extends PageBase {
  componentWillMount() {
    this.props.setTitle("咨询");
    this.state = {files:[],defaultFile:'',progress:false};//状态
  }

  handleClick() {
    if (XUtil.isEmpty(this.state['content_input'])) {
      showToast("请输入咨询内容");
    } else {
      let files = this.state.files.map(item => {
        return item.file;
      });
      this.setState({progress: true});
      addConsult(files, this.state['content_input']).then(result => {
        showToast("您的咨询已提交");
        location.replace('#/ask');
      });
    }
  }

  handleFileClick(comId, index) {
    if (comId === 'add_file') {
      this.refs.addFile.click();
    } else {
      let files = this.state.files;
      files.splice(index, 1);
      this.setState({files});
    }
  }

  handleLoad(comId, name, image, event) {
    switch (comId) {
      case 'reader_load':
        image.src = event.currentTarget.result;
        break;
      case 'image_load_add':
      {
        this.state.files.push({file:XUtil.compressImage(event.currentTarget), name:name});
        this.setState({files:this.state.files});
      }
        break;
      case 'image_load_edit':
      {
        let _files = this.state.files;
        _files[image] = {file:XUtil.compressImage(event.currentTarget), name:name};
        this.setState({files:_files});
      }
        break;
    }
  }

  handleChange(key, index, event) {
    let files = event.currentTarget.files;
    if (files && files.length > 0) {
      let file = files[0];
      let _files = this.state.files;
      if (file.size > 2 * 1024 * 1024) {//验证不通过
        showToast('单个文件不能超过2M');
        this.setState({defaultFile:''});
        return;
      } else if (_files.length > 9) {//验证不通过
        showToast("最多只能添加10个附件");
        this.setState({defaultFile:''});
        return;
      }
      let reader = new FileReader();
      let image = new Image();
      if (key === 'file_input_add') {//添加附件
        image.onload = this.handleLoad.bind(this, 'image_load_add', file.name, -1);//添加文件
      } else {//编辑附件
        image.onload = this.handleLoad.bind(this, 'image_load_edit', file.name, index);//编辑附件
      }
      reader.onload = this.handleLoad.bind(this, 'reader_load', null, image);
      reader.readAsDataURL(file);
    }
  }

  render() {
    return (
      <Container fill direction="column">
        <img src={require("../../image/consult-bg.png")}/>
        <Container
          scrollable
          className="white-background padding flex-1"
        >
          <Field
            label="问题描述"
            placeholder="请尽可能详细描述您的问题。"
            type="textarea"
            onChange={this.defaultHandleChange.bind(this, "content_input")}
            defaultValue={this.state['content_input']}
          />
          <div className="margin-bottom-xs">添加附件</div>
          {
            this.state.files.map((file, i) => {
              return (
                <label className="flex-row" key={'file_'+i}>
                  <Button amSize="xs" className="file-btn" amStyle="primary">选择文件</Button>
                  <input id={'file_'+i} type="file" accept="image/*" style={{display: "none"}}
                         onChange={this.handleChange.bind(this, "file_input_edit",i)}/>
                  <span className="flex-1 consult-span">{file.name}</span>
                  <Icon name="close" onClick={this.handleFileClick.bind(this, "file_input_del", i)}/>
                </label>
              );
            })
          }
          <label className="flex-row">
            <Button amSize="xs" amStyle="primary" className="file-btn" onClick={this.handleFileClick.bind(this, 'add_file')}>选择文件</Button>
            <input ref="addFile" type="file" accept="image/*" style={{display: "none"}} value={this.state.defaultFile} onChange={this.handleChange.bind(this, "file_input_add",-1)}/>
            <span className="flex-1 consult-span">添加附件</span>
          </label>
        </Container>
        <Group
          className="margin-0"
          component="span">
          <Button amStyle="primary" disabled={this.state.progress} block onClick={this.handleClick.bind(this)}>提交</Button>
        </Group>
      </Container>
    )
  }

}

export default connect(null, dispatch => bindActionCreators({setTitle}, dispatch))(Consult);
