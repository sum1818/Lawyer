import PageBase from "./PageBase";
import React from "react";
import {Container, Group, Button, Icon, List, Field, Modal} from "amazeui-touch";
import * as XUtil from "../utils/XUtil";
import showToast from "../utils/ToastUtil";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {vipState,activeVipCard} from '../apis/UserApi'

export default class Vip extends PageBase {

  componentWillMount() {
    document.title = "服务卡激活";
    this.state = {tel:'', userName:'', address: '', cardNumber:''};
    vipState().then(result => {
      if (!result.code) {
        if (result.state == 1) {
          location.replace("#/attention");
        }
      }
    });
  }

  handleClick(comId) {
    if (comId == 'submit'){
      //校验下数据格式
      if (!XUtil.isNumber(this.state.cardNumber)) {
        showToast("服务卡卡号输入不正确");
        return;
      } else if (!XUtil.isMobile(this.state.tel)) {
        showToast("请输入正确的手机号");
        return;
      }
      activeVipCard(this.state.cardNumber, this.state.userName, this.state.tel, this.state.address).then(result => {
        if (result.code == 0) {
          showToast(result.result);
          location.replace("#/attention");
        } else {
          showToast(result.error);
        }
      });
    }
  }

  render() {
    let checkForm = !XUtil.isEmpty(this.state.cardNumber)&&!XUtil.isEmpty(this.state.userName) && !XUtil.isEmpty(this.state.tel) && !XUtil.isEmpty(this.state.address);
    return (
      <Container fill direction="column">
        <Group
          header="服务卡激活"
          className="margin-0"
          component="span"
          noPadded
        />
        <Container
          scrollable>
          <Group className="margin-0">
            <Field
              label="卡号"
              placeholder="请输入您服务卡卡号"
              defaultValue={this.state['cardNumber']}
              onChange={this.defaultHandleChange.bind(this, 'cardNumber')}
            />
            <Field
              label="联系人"
              placeholder="请输入您的姓名或公司名称"
              defaultValue={this.state['userName']}
              onChange={this.defaultHandleChange.bind(this, 'userName')}
            />
            <Field
              label="联系电话"
              placeholder="请输入收件人手机号码"
              defaultValue={this.state['tel']}
              onChange={this.defaultHandleChange.bind(this, 'tel')}
            />
            <Field
              label="寄送地址"
              type="textarea"
              placeholder="请输入寄送地址"
              onChange={this.defaultHandleChange.bind(this, 'address')}
            />
          </Group>
        </Container>
        <Group
          className="margin-0"
          component="span">
          <Button amStyle="primary" block disabled={!checkForm} onClick={this.handleClick.bind(this,'submit')}>立即激活</Button>
        </Group>
      </Container>
    );
  }
}
