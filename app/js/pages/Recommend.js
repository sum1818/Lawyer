import PageBase from './PageBase';
import React from "react";
import {
  Container,
  Group,
  Button,
  Field
} from 'amazeui-touch';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle} from "../actions/ActionCreator";
import {postRecommend} from "../apis/UserApi";
import * as XUtil from '../utils/XUtil'
import showToast from "../utils/ToastUtil";

class Recommend extends PageBase {

  componentWillMount() {
    this.props.setTitle("意见反馈");
    this.state = {progress: false, recommend: ''};//
  }

  handleClick() {
    if (XUtil.isEmpty(this.state.recommend)) {
      showToast("请输入您的建议和反馈");
      return;
    }
    this.setState({progress:true});
    postRecommend(this.state.recommend).then(result => {
      this.setState({progress:false});
      showToast("感谢您的建议");
      history.back();
    });//
  }

  render() {
    return (
      <Container fill direction="column">
        <div className="white-background padding flex-1">
            <Field type="textarea" placeholder="请输入您的建议和反馈" defaultValue={this.state.recommend} onChange={this.defaultHandleChange.bind(this, 'recommend')}/>
          <Group
            className="margin-0"
            component="span">
            <Button amStyle="primary" disabled={this.state.progress} block onClick={this.handleClick.bind(this)}>提交</Button>
          </Group>
        </div>
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle}, dispatch))(Recommend);
