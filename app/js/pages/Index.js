import React from 'react';
import {
  Container,
  List,
  NavBar,
  Group,
  View,
  Slider
} from 'amazeui-touch';
import * as informationApi from '../apis/InformationApi';
import * as publicApi from '../apis/PublicApi';
import * as XUtil from '../utils/XUtil';
import {ASSETS_HOST} from '../const';

export default class Index extends React.Component {

  static contextTypes = {//注入router对象
    router: React.PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.state = {indexNews: [], indexBanners: [], lawyers:[]};
  }

  componentWillMount() {
    document.title = "纳古法云";
    informationApi.getIndexInfos().then(data => {
      if (data.news != null) {
        this.setState({indexNews:data.news.list});
      }
      if (data.banners != null) {
        this.setState({indexBanners:data.banners.list});
      }
    });
    publicApi.getIndexLawyers().then(data => {
      if (data.code == 0) {
        this.setState({lawyers: data.list});
      }
    });
  }

  handleTabClick() {
    this.context.router.push({pathname:'/searchLawyer'});
  }

  handleClick(comId, item, event) {
    switch (comId) {
      case 'news-detail':
        this.context.router.push({pathname:'/newsDetail', state:{url: item.url}});//跳转
        break;
      case 'lawyer-detail':
          this.context.router.push({pathname:'/lawyerDetail',state: {id: item.id}});
        break;
      case 'lawyer-chat':
        this.context.router.push({pathname:'/messageDetail', state: {referId:item.id, utype:1,name: item.name}});
        break;
    }
    event.stopPropagation();
  }

  render() {
    const rightNav = {
      className:"base-icon-search-white navbar-icon"
    };
    return (
      <View>
        <NavBar
          title="纳古法云"
          amStyle="dark"
          onAction={this.handleTabClick.bind(this)}
          rightNav={[rightNav]}
        />
        <Container scrollable>
          <Slider controls={false} className="slide-box">
            {
              this.state.indexBanners.map((item,i) => {
                return (
                  <Slider.Item key={"banner"+i}>
                    <a href={item.url}>
                      <img src={item.image}/>
                    </a>
                  </Slider.Item>
                )
              })
            }
          </Slider>
          <List className="margin-0 index_news_list">
            {
              this.state.indexNews.map((item,i) => {
                return (
                  <List.Item key={"indexNews" + i} className="padding-v-xs padding-h-sm">
                    <div className="item-main match-parent-width ms-size" onClick={this.handleClick.bind(this, 'news-detail', item)}>
                      <div className="padding-h-xs block-div">
                        <div className="title">{item.title}</div>
                        <div className="sm-size">{XUtil.convertDate(new Date(item.time*1000), 'YYYY-MM-DD')}</div>
                      </div>
                      <div><img src={ASSETS_HOST+item.image}/></div>
                    </div>
                  </List.Item>
                )
              })
            }
          </List>
          <List className="margin-0 index_lawyer_list base-background">
            <List.Item role="header" className="padding-h-0 padding-v-sm">
              <div className="item-main">
                <div className="mark-down margin-right-sm"/>
                <div className="ms-size dark-color flex-1">推荐律师</div>
                <div className="margin-right-xs"><a className="middle-href" href="#/findLawyer">查看更多<i className="base-icon-right-red small-icon"/></a></div>
              </div>
            </List.Item>
            {
              this.state.lawyers.map((lawyer,i) => {
                return (
                  <List.Item key={'lawyer_'+ i} className="padding-0 margin-h-xsm margin-bottom-sm white-background">
                    <div className="item-main padding-h-xs padding-v-sm" onClick={this.handleClick.bind(this, 'lawyer-detail', lawyer)}>
                      <div><img src={lawyer.head?(ASSETS_HOST+lawyer.head):require("../../image/news.png")}/></div>
                      <div className="block-div content">
                        <div className="item-main">
                          <div className="title flex-1">{lawyer.name}律师</div>
                          <div className="consult-btn" onClick={this.handleClick.bind(this, 'lawyer-chat', lawyer)}>咨询TA</div>
                        </div>
                        <div><span>执业: </span>{lawyer.years}</div>
                        <div><span>地域: </span>{lawyer.area}</div>
                        <div><span>专长: </span>{lawyer.specialty}</div>
                      </div>
                    </div>
                  </List.Item>
                )
              })
            }
          </List>
        </Container>
      </View>
    );
  }
}
