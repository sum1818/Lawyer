import PageBase from "./PageBase";
import React from "react";
import {Container, Group, List, Icon} from "amazeui-touch";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle, setMenu} from "../actions/ActionCreator";
import * as XUtil from "../utils/XUtil";

class MaterialType extends PageBase {

  constructor() {
    super();
    this.state = {index: 0};
  }

  componentWillMount() {
    this.props.setTitle("阶段");
    this.props.setMenu(null, null);
    this.state = Object.assign(this.state, this.props.location.state);
  }

  handleClick(comId, item, index) {
    switch (comId) {
      case 'type_item':
        this.setState({index:index});
        XUtil.setItem("selectMaterialType", JSON.stringify({title:item.title,index}));
        history.back();
        break;
    }
  }

  render() {
    const items = [
      {title: "一审"},{title: "二审"},{title: "执行"},{title: "收案"},{title: "其它"}
    ];
    return (
      <Container>
        <List className="margin-0 list-small-size">
          {
            items.map((item, i) => {
              return (<List.Item key={`key${i}`} onClick={this.handleClick.bind(this, 'type_item', item, i)} title={item.title} after={i == this.state.index ? <Icon name="check"/>:""}/>);
            })
          }
        </List>
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu}, dispatch))(MaterialType);
