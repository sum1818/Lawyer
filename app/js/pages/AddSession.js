import PageBase from "./PageBase";
import React from "react";
import {Container, Group, List, Field, Grid, Col} from "amazeui-touch";
import {connect} from "react-redux";
import { Link } from 'react-router';
import {bindActionCreators} from "redux";
import {setTitle, setMenu} from "../actions/ActionCreator";
import * as XUtil from "../utils/XUtil";
import showToast from "../utils/ToastUtil";
import {DateTimePicker} from '../widgets/datatimepicker/index';
import * as lawyerApi from "../apis/LawyerApi";
import {ASSETS_HOST} from '../const';

class AddSession extends PageBase {

  constructor() {
    super();
    this.state = {
      time: new Date(),
      isOpen: false,
      inUpload: false,
      files: []
    };
  }

  saveSession(session) {
    if (this.state.from == 'addCase') {
      this.editCase.session[this.editSession] = session;
    } else {
      this.editCase.session.push(session);
    }
    XUtil.setItem("editCase", JSON.stringify(this.editCase));
    history.back();
  }

  componentWillMount() {
    this.props.setTitle("开庭信息");
    this.editCase = JSON.parse(XUtil.getItem("editCase")) || {};
    this.editSession = 0;
    if (!this.editCase.session) {
      this.editCase.session = [];
    }
    if (this.props.location.state != null) {
      this.state.from = this.props.location.state.from;
      let session = this.props.location.state.session;
      let sessionArr = session.split("|");
      this.state.type = {};
      this.state.type['title'] = sessionArr[0];
      this.state.type['index'] = parseInt(sessionArr[1]);
      this.state.time = new Date(parseInt(sessionArr[2]));
      this.state.title = XUtil.convertDate(this.state.time, 'YYYY-MM-DD hh:mm');
      this.state['place_input'] = sessionArr[3];
      if (sessionArr.length > 4) {
        let files = sessionArr.slice(4);
        files = files.map(file => {
          return {url:file, image:<img src={`${ASSETS_HOST}/${file}`}/>};
        });
        this.state.files.extend(files);
      }
      //确定编辑的位置
      for(let i =0; i< this.editCase.session.length;i++) {
        let str = this.editCase.session[i];
        if (str == session) {
          this.editSession = i;
        }
      }
    }
    this.props.setMenu("保存", null, ()=>{
      if (!this.state.type) {
        showToast("请选择阶段");
      } else if (!this.state.title) {
        showToast("请选择开庭时间");
      } else if (XUtil.isEmpty(this.state['place_input'])) {
        showToast("开庭地点不能为空");
      } else if (this.state.inUpload) {
        showToast("请等待传票上传");
      } else {
        let session = `${this.state.type.title}|${this.state.type.index}|${this.state.time.valueOf()}|${this.state['place_input']}`;
        let sessionOrg = session;
        let uploadFiles = [];
        this.state.files.forEach(file => {
          if (!file.url) {
            uploadFiles.push(file.file);
          }
          session += `|${file.url}`;
        });
        if (uploadFiles.length > 0) {//先上传文件后再保存
          this.state.inUpload = true;
          lawyerApi.uploadSessionFiles(uploadFiles)
            .then(data => {
              this.state.inUpload = false;
              if (data.code == 0) {
                let list = data.list;
                let index = 0;
                this.state.files.forEach(file => {//保存文件
                  if (!file.url) {
                    file.url = list[index];
                    index ++;
                  }
                  sessionOrg += `|${file.url}`;
                });
                this.saveSession(sessionOrg);
              } else {
                showToast("传票上传失败");
              }
            });
        } else {
          this.saveSession(session);
        }
      }
    });
    let type = XUtil.getItem("selectSessionType");
    if (type != null) {
      XUtil.removeItem("selectSessionType");
      this.setState({type: JSON.parse(type)});
    }
  }

  handleClick(comId) {
    switch (comId) {
      case 'session_time':
        this.setState({ isOpen: true });
        break;
      case 'upload_file':
        this.fileInput.click();
    }
  }

  handleCancel() {
    this.setState({ isOpen: false });
  }

  handleSelect(time) {
    this.setState({ time, isOpen: false, title: XUtil.convertDate(time, 'YYYY-MM-DD hh:mm')});
  }

  handleLoad(comId, image, event) {
    switch (comId) {
      case 'reader_load':
        image.src = event.currentTarget.result;
        break;
      case 'image_load':
        this.state.files.push({file:XUtil.compressImage(event.currentTarget), image:(<img src={event.currentTarget.src}/>)});
        this.setState({files:this.state.files});
        break
    }
  }

  handleChange(event) {
    let file = event.currentTarget.files[0];
    if (this.state.files.length > 4) {
      showToast("最多添加5张图片");
      return false;
    } else if(!/image\/\w+/.test(file.type)){
      showToast("请选择图片文件上传");
      return false;
    } else {
      let reader = new FileReader();
      let image = new Image();
      image.onload = this.handleLoad.bind(this, 'image_load', null);
      reader.onload = this.handleLoad.bind(this, 'reader_load', image);
      reader.readAsDataURL(file);
    }
  }

  render() {
    const type = this.state.type;
    const title = this.state.title;
    return (
      <Container>
        <List className="margin-0 list-small-size">
          <List.Item linkComponent={Link} linkProps={{to: {pathname: "/sessionType", state: {from: "addSession", index: type?type.index:0}}}} title="阶段" after={type?(type.title):"请选择阶段"} />
          <List.Item className="base-background sm-size padding-v-xs" role="header">开庭前一天及当前7:00将提醒您</List.Item>
          <List.Item linkComponent={Link} onClick={this.handleClick.bind(this, 'session_time')} title="开庭时间" after={title||"请选择开庭时间"} />
          <List.Item nested="input" title="开庭地点">
            <Field
              className="flex-3 text-right no-arrow"
              placeholder="请输入开庭地点"
              onChange={this.defaultHandleChange.bind(this, "place_input")}
              value={this.state['place_input']||""}
            />
          </List.Item>
          <List.Item className="base-background sm-size padding-v-xs" role="header">拍传票</List.Item>
        </List>
        <Grid avg={4} className="text-center white-background padding-bottom-xs">
          {
            this.state.files.map((file, i) => {
              return (
                <Col key={"file" + i} shrink={true} className="padding-top-sm padding-bottom-0 padding-h-xs">
                  <a className="pic-take-btn">{file.image}</a>
                </Col>
              );
            })
          }
          <Col shrink={true} className="padding-top-sm padding-bottom-0 padding-h-xs">
            <a className="pic-take-btn base-icon-add" onClick={this.handleClick.bind(this, 'upload_file')}/>
          </Col>
        </Grid>
        <input type="file" onChange={this.handleChange.bind(this)} ref={input => {this.fileInput = input;}} accept="image/jpg,image/jpeg,image/png" className="base-hide" capture="camera" />
        <DateTimePicker
          value={this.state.time}
          isOpen={this.state.isOpen}
          theme="ios"
          onSelect={this.handleSelect.bind(this)}
          onCancel={this.handleCancel.bind(this)} />
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu}, dispatch))(AddSession);
