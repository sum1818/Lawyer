import PageBase from "./PageBase";
import React from "react";
import {Container, Group, List, Icon} from "amazeui-touch";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle, setMenu} from "../actions/ActionCreator";
import * as XUtil from "../utils/XUtil";

class DelegateUser extends PageBase {

  constructor() {
    super();
    this.state = {flag: 0};
  }

  componentWillMount() {
    this.props.setTitle("我方代理");
    this.editCase = JSON.parse(XUtil.getItem("editCase")) || {};
    this.props.setMenu("保存", null, ()=>{
      XUtil.setItem("editCase", JSON.stringify(this.editCase));
      history.back();
    });
  }

  handleClick(comId, party, index) {
    switch (comId) {
      case 'type_item':
        let partyArr = party.split("|");
        if (partyArr.length == 4) {
          partyArr.push(1);
        } else {
          partyArr[4] = parseInt(1 - partyArr[4]);
        }
        this.editCase.party[index] = partyArr.join("|");
        this.setState({flag:!this.state.flag});//翻转下数据触发Render方法
        break;
    }
  }

  render() {
    const parties = this.editCase.party;
    return (
      <Container>
        <List className="margin-0 list-small-size">
          {
            parties && parties.map((party, i) => {
              let partyArr = party.split("|");
              let selected = partyArr.length > 4 ? Boolean(parseInt(partyArr[4])) : false;
              return (<List.Item key={`key${i}`} onClick={this.handleClick.bind(this, 'type_item', party, i)} title={partyArr[3] + ":" + partyArr[0]} after={selected ? <Icon name="check"/>:""}/>);
            })
          }
        </List>
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu}, dispatch))(DelegateUser);
