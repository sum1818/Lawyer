import PageBase from './PageBase';
import React from "react";
import {
  Container,
  Group,
  Button,
  List
} from 'amazeui-touch';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle} from "../actions/ActionCreator";
import {listConsults} from "../apis/UserApi";
import * as XUtil from "../utils/XUtil";

class Ask extends PageBase {

  static contextTypes = {//注入router对象
    router: React.PropTypes.object.isRequired,
  };

  componentWillMount() {
    this.props.setTitle("咨询");
    this.state = {consults: []};
    listConsults(0).then(data => {
      if (data.code == 0) {
        this.setState({consults: data.list});
      }
    });
  }

  handleScroll(event) {
    let wholeHeight=event.currentTarget.scrollHeight;
    let scrollTop=event.currentTarget.scrollTop;
    let divHeight=event.currentTarget.clientHeight;
    if(divHeight+scrollTop>=wholeHeight) {//加载更多数据
      let length = this.state.consults.length;
      let lastId = length > 0 ? this.state.consults[length - 1].id : 0;
      listConsults(lastId).then(data => {
        if (data.code == 0) {
          let consults = this.state.consults;
          consults.extend(data.list);
          this.setState({consults});
        }
      });
    }
  }

  handleClick(comId, attachment) {
    if (comId == 'submit') {
      location.href = "#/consult";
    } else if (attachment){//附件预览
      // location.href = ASSETS_HOST + attachment;
      this.context.router.push({pathname:'/consultPreview', state: {attachment}});
    }
  }

  render() {
    return (
      <Container fill direction="column">
        <Group
          header="我的咨询"
          className="margin-0"
          component="span"
          noPadded
        />
        <Container
          onScroll={this.handleScroll.bind(this)}
          scrollable>
          <List className="ask-list margin-0 base-background">
            {
              this.state.consults.map(item => {
                return (
                  <List.Item key={item.id} className="white-background margin-sm" onClick={this.handleClick.bind(this, 'item', item.attachment)}>
                    <Container direction="column">
                      <div className="flex-row"><h4 className="flex-1">提问:</h4><label>{XUtil.convertDate(new Date(item.createtime*1000), "YYYY-MM-DD hh:mm")}</label></div>
                      <div className="content">{item.content}</div>
                      <div className="flex-row"><h4 className="flex-1">回复:</h4><label>{item.replytime?XUtil.convertDate(new Date(item.replytime*1000), "YYYY-MM-DD hh:mm"):""}</label></div>
                      <div className="content">{item.reply||'暂无回答'}</div>
                    </Container>
                  </List.Item>
                )
              })
            }
          </List>
        </Container>
        <Group
          className="margin-0"
          component="span">
          <Button amStyle="primary" block onClick={this.handleClick.bind(this, 'submit')}>我要咨询</Button>
        </Group>

      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle}, dispatch))(Ask);
