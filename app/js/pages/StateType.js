import PageBase from "./PageBase";
import React from "react";
import {Container, Group, List, Icon} from "amazeui-touch";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle, setMenu} from "../actions/ActionCreator";
import {caseStates} from "../const";
import * as XUtil from "../utils/XUtil";

class StateType extends PageBase {

  constructor() {
    super();
    this.state = {index: 0};
  }

  componentWillMount() {
    this.props.setTitle("阶段");
    this.props.setMenu(null, null);
    this.state = Object.assign(this.state, this.props.location.state);
  }

  handleClick(comId, item) {
    switch (comId) {
      case 'type_item':
        this.setState({index:item.index});
        let editCase = Object.assign({},JSON.parse(XUtil.getItem("editCase")),{state:item});
        XUtil.setItem("editCase", JSON.stringify(editCase));
        history.back();
        break;
    }
  }

  render() {
    const items = [];
    for(let i in caseStates){
      if (caseStates.hasOwnProperty(i)) {
        items.push({title: caseStates[i],index:i});
      }
    }
    return (
      <Container>
        <List className="margin-0 list-small-size">
          {
            items.map((item, i) => {
              return (<List.Item key={`key${i}`} onClick={this.handleClick.bind(this, 'type_item', item)} title={item.title} after={item.index == this.state.index ? <Icon name="check"/>:""}/>);
            })
          }
        </List>
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu}, dispatch))(StateType);
