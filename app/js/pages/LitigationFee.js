/**
 * Created by zl on 2017/11/11.
 */
import PageBase from './PageBase';
import React from "react";
import {
  Container,
  Group,
  Field,
  Button
} from 'amazeui-touch';
import * as XUtil from '../utils/XUtil'
import showToast from '../utils/ToastUtil'

const commonFees = [
  {ext: ['10000-50', '100000-0.025', '200000-0.02', '500000-0.015', '1000000-0.01', '2000000-0.009', '5000000-0.008', '10000000-0.007', '20000000-0.006', '-0.005']},
  {common: '50~100元'},
  {common: '50~300元', ext: ['200000-50-300', '-0.005']},
  {common: '100~500元', ext: ['50000-100-500', '100000-0.01', '-0.005']},
  {common:'500~1000元', ext: ['10000-50', '100000-0.025', '200000-0.02', '500000-0.015', '1000000-0.01', '2000000-0.009', '5000000-0.008', '10000000-0.007', '20000000-0.006', '-0.005']},
  {common: '10元'},
  {common: '50元'},
  {common: '100元'},
  {common: '50~100元'}
];

const execFee = {common: '50~500元', ext: ['10000-50', '500000-0.015', '5000000-0.01', '10000000-0.005', '0.001']};

const keepFee = {common: '30元', ext: ['1000-30', '100000-0.01', '-0.005']};

export default class LitigationFee extends PageBase {

  componentWillMount() {
    document.title = '诉讼费计算';
    this.state = {type: 0, money: '', show: false, result: ''};
  }

  handleClick() {
    if ((this.state.type==0||this.state.type==2||this.state.type==3||this.state.type==4) && this.state.money.length > 15) {
      showToast('标的金额最多只能保留15位数字');
      return;
    } else if (this.state.type==0 && !XUtil.isNumber(this.state.money)) {
      showToast('标的金额必须为整数');
      return;
    }
    let result = '';
    let type = this.state.type;
    let commonFee = commonFees[type];
    if (!commonFee.ext || !XUtil.isNumber(this.state.money)) {
      result = `受理费：${commonFee.common}, 执行费：${execFee.common}, 保全费：${keepFee.common}`;
    } else {
      let extendFee = commonFee.ext;
      let money = parseInt(this.state.money);
      let costMoney = 0;
      let lastLimit = 0;
      let firstDiff = 0;
      for (let item of extendFee) {
        let limits = item.split('-');
        let limitMoney = parseInt(limits[0]);
        let rate = parseFloat(limits[1]);
        if (limits.length > 2) {
          firstDiff = parseFloat(limits[2]) - rate;
        }
        if (money > limitMoney) {
          if (rate > 1) {
            costMoney += rate;
          } else {
            costMoney += (limitMoney - lastLimit) * rate;
          }
        } else {
          if (rate > 1) {
            costMoney += rate;
          } else {
            costMoney += (money - lastLimit) * rate;
          }
          break;
        }
        lastLimit = limitMoney;
      }
      if (firstDiff > 0) {
        let maxCostMoney = costMoney + firstDiff;
        result += `受理费：${costMoney.toFixed(3)}~${maxCostMoney.toFixed(3)}元, `;
      } else {
        result = `受理费：${costMoney.toFixed(3)}元, `;
      }
      let execMoney = 0;
      lastLimit = 0;
      for (let item of execFee.ext) {
        let limits = item.split('-');
        let limitMoney = parseInt(limits[0]);
        let rate = parseFloat(limits[1]);
        if (money > limitMoney) {
          if (rate > 1) {
            execMoney += rate;
          } else {
            execMoney += (limitMoney - lastLimit) * rate;
          }
        } else {
          if (rate > 1) {
            execMoney += rate;
          } else {
            execMoney += (money - lastLimit) * rate;
          }
          break;
        }
        lastLimit = limitMoney;
      }
      result += `执行费：${execMoney.toFixed(3)}元, `;
      let keepMoney = 0;
      lastLimit = 0;
      for (let item of keepFee.ext) {
        let limits = item.split('-');
        let limitMoney = parseInt(limits[0]);
        let rate = parseFloat(limits[1]);
        if (money > limitMoney) {
          if (rate > 1) {
            keepMoney += rate;
          } else {
            keepMoney += (limitMoney - lastLimit) * rate;
          }
        } else {
          if (rate > 1) {
            keepMoney += rate;
          } else {
            keepMoney += (money - lastLimit) * rate;
          }
          break;
        }
        lastLimit = limitMoney;
      }
      if (keepMoney > 5000) keepMoney = 5000;
      result += `保全费：${keepMoney.toFixed(3)}元。 `;//
    }
    this.setState({show: true, result});
  }

  render() {
    let checkForm = !(this.state.type==0&&!XUtil.isNumber(this.state.money));
    return (
      <Container scrollable className="white-background">
        <Group className="margin-0">
          <Field
            type="select"
            label="请选择案件类型"
            ref="select"
            onChange={this.defaultHandleChange.bind(this, 'type')}
            defaultValue={this.state.type}
          >
            <option value="0">财产案件</option>
            <option value="1">普通非财产案件</option>
            <option value="2">离婚案件</option>
            <option value="3">人格权的案件</option>
            <option value="4">知识产权案件</option>
            <option value="5">劳动争议案件</option>
            <option value="6">普通行政案件</option>
            <option value="7">商标、专利、海事行政案件</option>
            <option value="8">管辖权异议不成立案件</option>
          </Field>
          {
            (this.state.type==0||this.state.type==2||this.state.type==3||this.state.type==4)?(
                <Field
                  label="标的金额"
                  placeholder="请输入标的金额（单位:元)"
                  type="number"
                  maxLength="15"
                  onChange={this.defaultHandleChange.bind(this, 'money')}
                  defaultValue={this.state.money}
                />
              ):null
          }
        </Group>
        {
          this.state.show? (
              <Group className="margin-v-0">
                计算结果：{this.state.result}
              </Group>
            ):null
        }
        <Group className="margin-v-0">
          提示：诉讼费计算结果仅供参考，具体已实际收费为准。
        </Group>
        <Group
          className="margin-0"
          component="span">
          <Button amStyle="success" block disabled={!checkForm} onClick={this.handleClick.bind(this,'submit')}>开始计算</Button>
        </Group>
      </Container>
    );
  }
}
