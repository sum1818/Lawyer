/**
 * Created by zl on 2017/10/14.
 */
import PageBase from './PageBase';
import React from "react";
import {
  Container
} from 'amazeui-touch';
import * as informationApi from '../apis/InformationApi';

class Introduce extends PageBase {

  componentWillMount() {
    document.title = "服务律所简介";
    this.state = {lastScale: 1, currentScale: 1, currentX: 0 , currentY: 0};
    informationApi.getUrlContent("/information/introduce.html").then(data => {//
      this.setState({content: data});
    });
  }
  formatTransform(offx, offy, scale) {
    this.state.currentX = offx;
    this.state.currentY = offy;
    this.state.lastScale = scale;
    alert(offx);//
    let translate = 'translate3d(' + (offx + 'px,') + (offy + 'px,') + '0)',
      myScale = 'scale(' + scale + ')';
    return translate + ' ' + myScale;
  }


  componentDidMount() {
    let container = document.getElementById("container");//
    new PinchZoom(container, {use2d: false});
  }

  render() {
    const bgStyle = {
      background: "#FFFACD"
    }
    return (
      <Container id="container" className="pinch-zoom" fill scrollable style={bgStyle} {...this.props} dangerouslySetInnerHTML={{__html: this.state.content}}/>
    );
  }
}

export default Introduce;
