import PageBase from './PageBase';
import React from "react";
import {Container, List, Field, Button} from "amazeui-touch";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle,setMenu} from "../actions/ActionCreator";
import * as adminApi from "../apis/AdminApi";
import * as publicApi from "../apis/PublicApi";
import * as XUtil from "../utils/XUtil";
import showToast from "../utils/ToastUtil";

class ModifyLawyer extends PageBase {

  constructor(props) {
    super(props);
    this.state = {lawyerId:props.location.state.lawyerId};
    publicApi.lawyerDetail(this.state.lawyerId)
      .then(data => {
        if (data.id) {
          for (let i in data) {
            if (data.hasOwnProperty(i)) {
              this.state[i + "_input"] = data[i];
            }
          }
          this.setState(this.state);
        }
      });
  }

  componentWillMount() {
    this.props.setTitle("编辑律师");
    this.props.setMenu(null,null);
  }

  handleClick() {
    if (XUtil.isEmpty(this.state.name_input)) {
      showToast("请输入律师姓名");
    } else if (!XUtil.isMobile(this.state.mobile_input)){
      showToast("请输入律师手机号码");
    } else if (XUtil.isEmpty(this.state.years_input)) {
      showToast("请输入律师执业年数");
    } else if (XUtil.isEmpty(this.state.area_input)) {
      showToast("请输入律师所在工作地域");
    } else if (XUtil.isEmpty(this.state.specialty_input)) {
      showToast("请输入律师业务专长");
    } else if (XUtil.isEmpty(this.state.synopsis_input)) {
      showToast("请输入律师简介");
    } else {
      adminApi.modifyLawyer(this.state.lawyerId, this.state.name_input,this.state.mobile_input,this.state.years_input,this.state.area_input,this.state.specialty_input,this.state.synopsis_input)
        .then(data => {
          if (data.code == 0) {
            showToast(data.result);
            history.back();
          } else {
            showToast(data.error)
          }
        });
    }
  }

  render() {
    return (
      <Container scrollable>
        <List className="list-small-size margin-top-sm">
          <List.Item nested="input" title="律师姓名:">
            <Field
              className="flex-3"
              placeholder="请输入律师姓名"
              value={this.state.name_input||""}
              onChange={this.defaultHandleChange.bind(this, 'name_input')}
            />
          </List.Item>
          <List.Item nested="input" title="联系电话:">
            <Field
              className="flex-3"
              containerClassName="no-arrow"
              placeholder="请输入律师手机号码"
              type="number"
              value={this.state.mobile_input||""}
              onChange={this.defaultHandleChange.bind(this, 'mobile_input')}
            />
          </List.Item>
          <List.Item nested="input" title="执业:">
            <Field
              className="flex-3"
              containerClassName="no-arrow"
              placeholder="请输入律师执业年数"
              type="number"
              value={this.state.years_input||""}
              onChange={this.defaultHandleChange.bind(this, 'years_input')}
            />
          </List.Item>
          <List.Item nested="input" title="工作地域:">
            <Field
              className="flex-3"
              containerClassName="no-arrow"
              placeholder="请输入律师所在工作地域"
              type="text"
              value={this.state.area_input||""}
              onChange={this.defaultHandleChange.bind(this, 'area_input')}
            />
          </List.Item>
          <List.Item nested="input" title="业务专长:">
            <Field
              className="flex-3"
              containerClassName="no-arrow"
              placeholder="请输入律师业务专长"
              type="text"
              value={this.state.specialty_input||""}
              onChange={this.defaultHandleChange.bind(this, 'specialty_input')}
            />
          </List.Item>
          <List.Item nested="input" title="简介:">
            <Field
              className="flex-3"
              containerClassName="no-arrow"
              placeholder="请输入律师简介"
              type="textarea"
              value={this.state.synopsis_input||""}
              onChange={this.defaultHandleChange.bind(this, 'synopsis_input')}
            />
          </List.Item>
        </List>
        <div className="base-padding">
          <Button id="add_btn" amStyle="primary" onClick={this.handleClick.bind(this, "add_btn")} block>确定修改</Button>
        </div>
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu}, dispatch))(ModifyLawyer);
