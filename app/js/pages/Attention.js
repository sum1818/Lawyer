/**
 * Created by zl on 2017/12/25.
 */
import PageBase from "./PageBase";
import React from "react";
import {
  Container,
  Group,
  List,
  Icon,
  Button
} from 'amazeui-touch';

export default class Attention extends PageBase {

  componentWillMount() {
    document.title = "关注我们";
  }

  render() {
    return (
      <Container fill direction="column">
        <Group component="span" className="margin-0">
          <Icon name="check" className="green-color"/>
          恭喜您激活服务卡成功！请长按下面的二维码，识别后点击关注“纳古法云”微信公众号，建议您将此公众号置顶。
        </Group>
        <div className="base-center margin-top-lg">
          <img src={require("../../image/qrcode.jpg")}  />
        </div>
      </Container>
    );
  }

}
