import PageBase from "./PageBase";
import React from "react";
import {Container, List, Field} from "amazeui-touch";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle, setMenu, addMessage, insertMessage, saveCache} from "../actions/ActionCreator";
import {InputSubmitHandleListener} from "../handler/InputChangeHandler";
import * as messageApi from "../apis/MessageApi";
import * as XUtil from "../utils/XUtil";

class MessageDetail extends PageBase {

  static contextTypes = {//注入router对象
    router: React.PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      message_input: "",
      referId: 0
    };
    if (props.location.state != null) {
      this.state.referId = props.location.state.referId;
      this.state.referName = props.location.state.name;
      this.state.referType = props.location.state.utype;
      XUtil.setItem("currentReferId", this.state.referId);
      XUtil.setItem("currentReferType", this.state.referType);
      XUtil.setItem("currentReferName", this.state.referName);
    } else {
      this.state.referId = XUtil.getItem("currentReferId");
      this.state.referType = XUtil.getItem("currentReferType");
      this.state.referName = XUtil.getItem("currentReferName");
    }
    if (this.state.referType == 1) {//律师
      this.props.saveCache({detailLawyerId:this.state.referId});
    } else {//用户
      this.props.saveCache({detailCustomerId:this.state.referId});
    }
  }

  componentWillMount() {
    this.props.setTitle(this.state.referName);
    this.props.setMenu(null, "card", () => {
      if (this.state.referType == 1) {
        this.context.router.push({pathname:'/lawyerDetail',state: {id: this.state.referId}});
      } else {
        this.context.router.push({pathname:'/customerDetail',state: {id: this.state.referId}});
      }
    });
    const messageList = this.props.messages[this.state.referId] || [];
    let firstId = messageList.length > 0 ? messageList[0].id : 0;
    if (firstId == 0 || messageList.length < 30) {
      messageApi.privateMessageList(this.state.referId, firstId)
        .then(data => {
          if (data.code == 0) {
            let list = data.list;
            this.props.insertMessage(this.state.referId, list);
            this.scrollToBottom();
          }
        });
    }
    //清理未读数
    let unread = JSON.parse(XUtil.getItem("unread", "{}"));
    unread[this.state.referId] = 0;
    XUtil.setItem("unread", JSON.stringify(unread));
    let unreadTmp = JSON.parse(XUtil.getItem("unreadTmp", "{}"));
    unreadTmp[this.state.referId] = 0;
    XUtil.setItem("unreadTmp", JSON.stringify(unreadTmp));
    this.clearUnRead();
    this.scrollToBottom();
  }

  clearUnRead() {

  }

  componentWillUnmount() {
    this.clearUnRead();
  }

  scrollToBottom() {
    if (this.scrollList) {
      this.scrollList.scrollTop = this.scrollList.scrollHeight;
    } else {
      setTimeout(this.scrollToBottom.bind(this), 50);
    }
  }

  handleSubmit() {
    if (XUtil.isEmpty(this.state.message_input)) return;
    messageApi.sendPrivateMessage(this.state.referId, this.state.message_input)
      .then(data => {
        if (data.id > 0) {
          this.props.addMessage(this.state.referId, data);
          this.setState({message_input:""});
          this.scrollToBottom();
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  handleFocus() {
    this.scrollToBottom();
  }

  handleScroll() {
    if (this.scrollList.scrollTop == 0) {//加载历史纪录
      console.log("load the history");
    }
  }

  componentWillReceiveProps() {
    setTimeout(this.scrollToBottom.bind(this), 200);
  }

  render() {
    const messageList = this.props.messages[this.state.referId]||[];
    return (
      <div className="flex-column match-parent-height">
        <div className="flex-1 scroll-div" ref={(scrollList) => {this.scrollList = scrollList; }} onScroll={this.handleScroll.bind(this)}>
          <List className="margin-0 message-list padding-h-sm base-background">
            {
              messageList.map((message, index) => {
                let prevMessage = index > 0 ? messageList[index - 1] : null;
                let showDate;
                if (prevMessage != null) {
                  showDate = parseInt(prevMessage.sentTime / 300) != parseInt(message.sentTime / 300);
                } else {
                  showDate = true;
                }
                return message.referId == this.state.referId ? (
                    <List.Item key={message.id} className="padding-top-sm padding-bottom-xs padding-h-0 right-msg">
                      <div className="flex-1">
                        <div className={`message-date ${showDate?"":"base-hide"}`}>{XUtil.messageDate(message.sentTime)}</div>
                        <div className="flex-row-reverse">
                          <img className="message-head margin-left-sm" src={require("../../image/news.png")}/>
                          <div className="right-content">{message.message}</div>
                        </div>
                      </div>
                    </List.Item>
                  ):(
                    <List.Item key={message.id} className="padding-top-sm padding-bottom-xs padding-h-0">
                      <div className="flex-1">
                        <div className={`message-date ${showDate?"":"base-hide"}`}>{XUtil.messageDate(message.sentTime)}</div>
                        <div className="flex-row">
                          <img className="message-head margin-right-sm" src={require("../../image/news.png")}/>
                          <div className="left-content">{message.message}</div>
                        </div>
                      </div>
                    </List.Item>
                )
              })
            }
            {/*<List.Item className="padding-top-sm padding-bottom-xs padding-h-0">*/}
              {/*<div>*/}
                {/*<div className="message-date">3月18日 20:30</div>*/}
                {/*<div className="flex-row">*/}
                  {/*<img className="message-head margin-right-sm" src={require("../../image/news.png")}/>*/}
                  {/*<div className="left-content">文本消息文本消息文本消息文本消息文本消息</div>*/}
                {/*</div>*/}
              {/*</div>*/}
            {/*</List.Item>*/}
            {/*<List.Item className="padding-top-sm padding-bottom-xs padding-h-0">*/}
              {/*<div>*/}
                {/*<div className="message-date base-hide">3月18日 20:30</div>*/}
                {/*<div className="flex-row">*/}
                  {/*<img className="message-head margin-right-sm" src={require("../../image/news.png")}/>*/}
                  {/*<div className="left-content">文本消息文本消息文本消息文本消息文本消息</div>*/}
                {/*</div>*/}
              {/*</div>*/}
            {/*</List.Item>*/}
            {/*<List.Item className="padding-top-sm padding-bottom-xs padding-h-0 right-msg">*/}
              {/*<div>*/}
                {/*<div className="message-date">3月18日 20:30</div>*/}
                {/*<div className="flex-row">*/}
                  {/*<div className="right-content">文本消息文本消息文文本消息文本消息文文本消息文本消息文文本消息文本消息文文本消息文本消息文</div>*/}
                  {/*<img className="message-head margin-left-sm" src={require("../../image/news.png")}/>*/}
                {/*</div>*/}
              {/*</div>*/}
            {/*</List.Item>*/}
            {/*<List.Item className="padding-top-sm padding-bottom-xs padding-h-0">*/}
              {/*<div className="message-date match-parent-width">本次咨询已结束</div>*/}
            {/*</List.Item>*/}
            {/*<List.Item className="padding-top-sm padding-bottom-xs padding-h-0 right-msg">*/}
              {/*<div>*/}
                {/*<div className="message-date">3月18日 20:30</div>*/}
                {/*<div className="flex-row">*/}
                  {/*<div className="right-content">文本消息文本消息文文本消息文本消息文文本消息文本消息文文本消息文本消息文文本消息文本消息文</div>*/}
                  {/*<img className="message-head margin-left-sm" src={require("../../image/news.png")}/>*/}
                {/*</div>*/}
              {/*</div>*/}
            {/*</List.Item>*/}
            {/*<List.Item className="padding-top-sm padding-bottom-xs padding-h-0 right-msg">*/}
              {/*<div>*/}
                {/*<div className="message-date">3月18日 20:30</div>*/}
                {/*<div className="flex-row">*/}
                  {/*<div className="right-content">文本消息文本消息文文本消息文本消息文文本消息文本消息文文本消息文本消息文文本消息文本消息文</div>*/}
                  {/*<img className="message-head margin-left-sm" src={require("../../image/news.png")}/>*/}
                {/*</div>*/}
              {/*</div>*/}
            {/*</List.Item>*/}
          </List>
        </div>
        <div className="white-background">
          <Field placeholder="聊点什么..."
                 onChange={this.defaultHandleChange.bind(this, 'message_input')}
                 onFocus={this.handleFocus.bind(this)}
                 onKeyDown={InputSubmitHandleListener.bind(this, this.handleSubmit.bind(this))}
                 value={this.state.message_input}
                 className="field margin-v-0 message-input"/>
        </div>
        <div className="flex-row ms-size base-border-up base-hide">
          <div className="flex-2 padding-sm main-color white-background">提供简要咨询服务</div>
          <div className="flex-1 text-center padding-sm white-color main-background">咨询TA</div>
        </div>
      </div>
    );
  }
}

export default connect(state => {return {messages: state.SaveMessage}}, dispatch => bindActionCreators({setTitle,setMenu,addMessage,insertMessage,saveCache}, dispatch))(MessageDetail);
