import React from "react";
import {View, NavBar} from "amazeui-touch";
import {Link} from "react-router";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {showTab, setMenu} from "../actions/ActionCreator";
import PageCustomer from "./PageCustomer";
import AddCustomer from "./AddCustomer";
import CustomerDetail from "./CustomerDetail";
import CaseDetail from "./CaseDetail";
import AddCase from "./AddCase";
import PageLawyer from "./PageLawyer";
import AddLawyer from "./AddLawyer";
import LawyerDetail from "./LawyerDetail";
import ModifyNick from "./ModifyNick";
import ModifyMobile from "./ModifyMobile";
import ModifyRemark from "./ModifyRemark";
import BindMobile from "./BindMobile";
import About from "./About";
import CaseType from "./CaseType";
import AddParty from "./AddParty";
import PartyType from "./PartyType";
import VisibleType from "./VisibleType";
import AddSession from "./AddSession";
import SessionType from "./SessionType";
import AddMaterial from "./AddMaterial";
import MaterialType from "./MaterialType";
import DelegateUser from "./DelegateUser";
import StateType from "./StateType";
import SetPass from "./SetPass";
import FindLawyer from "./FindLawyer";
import MessageDetail from "./MessageDetail";
import ModifyLawyer from "./ModifyLawyer";
import NewsDetail from './NewsDetail';
import Protocol from "./Protocol";
import Recommend from "./Recommend"
import Consults from "./Consults"
import NotFound from "./NotFound";
import * as XUtil from "../utils/XUtil";

const pages = {
  ModifyNick,
  ModifyMobile,
  ModifyRemark,
  BindMobile,
  About,
  PageCustomer,
  AddCustomer,
  CustomerDetail,
  CaseDetail,
  AddCase,
  PageLawyer,
  AddLawyer,
  LawyerDetail,
  AddParty,
  PartyType,
  CaseType,
  VisibleType,
  AddSession,
  SessionType,
  AddMaterial,
  MaterialType,
  DelegateUser,
  StateType,
  SetPass,
  FindLawyer,
  MessageDetail,
  ModifyLawyer,
  NewsDetail,
  Protocol,
  Recommend,
  Consults
};

const authlessPages = {//无需登录的页面
  About,
  NewsDetail,
  FindLawyer,
  Protocol
}

class Page extends React.Component {

  static contextTypes = {//注入router对象
    router: React.PropTypes.object.isRequired,
  };

  componentWillMount() {
    this.props.showTab();
    //判断下登录状态
    let page = this.props.params.page;
    if (page) {
      page = page.charAt(0).toUpperCase() + page.slice(1);
    }
    if (!authlessPages[page]) {//需要登录
      if (XUtil.getItem("isLogin") != "true") {
        XUtil.replaceLogin();
      }
    }
  }
  componentWillUnmount() {
    this.props.showTab();
  }
  render() {
    let page = this.props.params.page;
    if (page) {
      page = page.charAt(0).toUpperCase() + page.slice(1);
    }
    let title = this.props.title;
    const Component = pages[page] || NotFound;

    let rightNav = (this.props.menuTitle || this.props.menuIcon) ? {
        component: Link,
        className: this.props.menuIcon?`base-icon-${this.props.menuIcon} navbar-icon`:"",
        title: this.props.menuTitle,
        type: 'menu'
      }: null;

    const clickHandler = (item, e) => {
      e.preventDefault();
      if (item.type == 'back') {//返回操作
        this.props.setMenu(null, null);
        this.props.showTab();
        this.context.router.goBack();
        // history.back();
      } else if (item.type == 'menu' && this.props.menuAction) {//按钮操作
        switch (typeof this.props.menuAction) {
          case 'function':
          {
            this.props.menuAction.call(item.page);
            break;
          }
          case 'string':
          {
            this.props.setMenu(null, null, null);
            if (this.props.menuAction == '#/pageLogin') {
              XUtil.replaceLogin();
            } else {
              location.href = this.props.menuAction;
            }
            break;
          }
        }
      }
    }

    let navTitle = {
      title: title,
      leftNav: [{component: Link, className:"base-icon-back navbar-icon", type: 'back'}],
      onAction: clickHandler
    };
    if (rightNav) {
      navTitle.rightNav = [rightNav];
    }
    return (//加载location以便传递参数
      <View>
        <NavBar {...navTitle}
                amStyle="dark"
        />
        <Component scrollable location={this.props.location}/>
      </View>
    );
  }
}

export default connect(state => state.PageTitle, dispatch => bindActionCreators({showTab, setMenu}, dispatch))(Page);
