import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle, setMenu, saveCache} from "../actions/ActionCreator";
import {
  Container,
  Group,
  List
} from 'amazeui-touch';
import {AddCaseOrg} from "./AddCase";
import * as lawyerApi from "../apis/LawyerApi";
import * as userApi from "../apis/UserApi";
import {caseStates,caseTypes,visibleTypes,partyTypes} from "../const";
import showToast from "../utils/ToastUtil";
import * as XUtil from "../utils/XUtil";

class CaseDetail extends AddCaseOrg {
  constructor(props) {
    super(props);
    if (props.location.action == "PUSH") {
      this.caseId = props.location.state.id;
      props.saveCache({detailCaseId:this.caseId});
      XUtil.removeItem("editCase");//清空之前的缓存
      XUtil.removeItem("editLoad");//清空之前的缓存
    } else {
      this.caseId = props.detailCaseId;
    }
  }
  componentWillMount() {
    this.props.setTitle("案件详情");
    this.props.setMenu("保存", null, ()=>{
      let data = this.validCaseData();//数据校验
      data && lawyerApi.modifyCase(data, this.caseId)
        .then(data => {
          if (data.code == 0) {
            showToast(data.result);
            XUtil.removeItem("editCase");
            XUtil.removeItem("editLoad");
            history.back();
          } else {
            showToast("您没有权限修改本案");
          }
        });
    });
    if (XUtil.getItem("editLoad")) {
      Object.assign(this.state, JSON.parse(XUtil.getItem("editCase")));
    } else {
      lawyerApi.caseDetail(this.caseId)
        .then(data => {
          if (data.code == 0) {
            this.state['year_input'] = data.casenum.substring(1,5);
            let wordNum = data.casenum.substring(6).split("字第");
            if (wordNum.length > 1) {
              this.state['word_input'] = wordNum[0];
              this.state['num_input'] = wordNum[1].substring(0, wordNum[1].length - 1);//绑定案号
            }
            this.state.type = {index:data.type,title:caseTypes[data.type]};
            this.state.visible = {index:data.looklevel, title:visibleTypes[data.looklevel]};
            this.state.state = {index:data.state, title:caseStates[data.state]};
            if (data.state == 20) {
              this.state.finishState = true;
            }
            this.state["reason_input"] = data.reason;
            this.state["news_input"] = data.news;
            let delegateDict = new Set();
            let delegateArr = data.delegates.split(";");
            delegateArr.forEach(delegate => {
              delegateDict.add(delegate.split("|")[0]);
            });
            if (data.parties != null && data.parties.length > 0) {//当事人信息
              data.parties.forEach(party => {
                if (delegateDict.has(party.name)) {
                  this.state.party.push(`${party.name}|${party.mobile}|${party.type}|${partyTypes[party.type]}|1`);
                } else {
                  this.state.party.push(`${party.name}|${party.mobile}|${party.type}|${partyTypes[party.type]}`);
                }
              })
            }
            if (data.sessions != null && data.sessions.length > 0) {//开庭信息
              this.state.session = data.sessions.split(";");
            }
            if (data.materials != null && data.materials.length > 0) {//材料信息
              this.state.material = data.materials.split(";");
            }
            this.state["remark_input"] = data.remark;
          }
          // this.state
          this.setState(this.state);
          XUtil.setItem("editCase", JSON.stringify(this.state));
          XUtil.setItem("editLoad", 1);
          return userApi.getReferInfo(data.guestid);
        })
        .then(data => {
          if (data.code == 0) {
            this.state.guest = data;
            this.setState(this.state);
            XUtil.setItem("editCase", JSON.stringify(this.state));
          }
        });
    }
  }
}
export default connect(state => state.SaveCache, dispatch => bindActionCreators({setTitle,setMenu,saveCache}, dispatch))(CaseDetail);
