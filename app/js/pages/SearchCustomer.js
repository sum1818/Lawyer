import React from "react";
import {Container, Group, List, Field, Button, Icon, View} from "amazeui-touch";
import {connect} from "react-redux";
import { Link } from 'react-router';
import {bindActionCreators} from "redux";
import {setTitle, setMenu, saveCache} from "../actions/ActionCreator";
import * as lawyerApi from "../apis/LawyerApi";
import showToast from "../utils/ToastUtil";
import SearchNav from "../widgets/SearchNav";
import {InputStateChangeHandler} from "../handler/InputChangeHandler"

class SearchCustomer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      guests: []
    };
    if (props.location.action == "PUSH") {
      this.state['customer_search'] = props.location.state.name;
      props.saveCache({'customer_search':this.state['customer_search']});
    } else {
      this.state['customer_search'] = props['customer_search'];
    }
  }

  componentWillMount() {
    this.searchGuest();
    document.title = "搜索";
  }

  searchGuest() {
    this.state.guests = [];
    lawyerApi.searchGuest(this.state['customer_search'])
      .then(data => {
        if (data.code == 0) {
          this.setState({guests: data.list});
        } else {
          showToast(data.error);
        }
      });
  }

  handleClick(comId) {
    switch (comId) {
      case 'cancel_btn':
        history.back();
        break;
      case 'search_btn':
        this.searchGuest();
        this.props.saveCache({'customer_search':this.state['customer_search']});
        break;
    }
  }

  render() {
    const items = [];
    this.state.guests.forEach(guest => {
      items.push((<List.Item key={guest.id} title={(guest.name || " ") + " (" + guest.mobile + ")"} linkComponent={Link} linkProps={{to: {pathname: "/customerDetail", state: guest}}}/>));
    });

    return (
      <View>
        <SearchNav
          onSubmit={this.handleClick.bind(this, 'search_btn')}
          onChange={InputStateChangeHandler.bind(this, 'customer_search')}
          onCancel={this.handleClick.bind(this, 'cancel_btn')}
          placeholder="请输入客户姓名..."
          value={this.state['customer_search']||""}
        />
        <Container  scrollable>
          <List className="margin-0 list-small-size">
            {items}
          </List>
        </Container>
      </View>
    );
  }
}

export default connect(state => state.SaveCache, dispatch => bindActionCreators({setTitle,setMenu,saveCache}, dispatch))(SearchCustomer);
