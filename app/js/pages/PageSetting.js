import PageBase from "./PageBase";
import React from "react";
import {Container, Group, List, Button, Field} from "amazeui-touch";
import {connect} from "react-redux";
import {Link} from "react-router";
import {bindActionCreators} from "redux";
import {setTitle,showPrompt} from "../actions/ActionCreator";
import * as userApi from "../apis/UserApi";
import showToast from "../utils/ToastUtil";
import PromptDialog from "../dialogs/PromptDialog";
import * as XUtil from "../utils/XUtil";
import {ASSETS_HOST} from '../const';

class PageSetting extends PageBase {

  constructor() {
    super();
    this.state = {
      userInfo:{
      }
    };
  }

  componentWillMount() {
    this.props.setTitle("个人信息");
    userApi.getUserInfo()
      .then(data => {
        if (data.code == 0) {
          this.setState({userInfo:data});
        } else {
          // showToast(data.error);
        }
      });
  }

  handleClick(comId) {
    switch (comId) {
      case 'logout_btn'://退出登录
        userApi.sendLogout()
          .then(data => {
            if (data.code == 0) {
              XUtil.removeItem("currentReferId");
              XUtil.replaceLogin();
            }
          });
        break;
      case 'nick_edit'://修改昵称
        this.props.showPrompt(null, data => {
          if (data != null) {
            let remark = data.trim();
            if (remark.length == 0) {
              showToast("请输入新的昵称");
            } else {
              userApi.modifyName(remark)
                .then(data => {
                  if (data.code == 0) {//修改名称成功
                    showToast("修改昵称成功");
                    this.state.userInfo.name = remark;
                    this.setState({userInfo:this.state.userInfo});
                  } else {
                    showToast(data.error);
                  }
                });
            }
          }
          return true;
        });
        break;
      case 'change_head':
        this.fileInput.click();
        break;
    }
  }

  handleLoad(comId, image, event) {
    switch (comId) {
      case 'reader_load':
        image.src = event.currentTarget.result;
        break;
      case 'image_load':
        let imageFile = XUtil.compressImage(event.currentTarget, 300, 300);//上传头像
        userApi.uploadHead(imageFile)
          .then(data => {
            if (data.code == 0) {
              this.state.userInfo.head = data.result;
              this.setState({userInfo:this.state.userInfo});
              showToast("更改头像成功");
            } else {
              showToast(data.error);
            }
          });
        break
    }
  }

  handleChange(event) {
    let file = event.currentTarget.files[0];
    if(!/image\/\w+/.test(file.type)){
      showToast("请选择图片文件上传");
      return false;
    } else {
      let reader = new FileReader();
      let image = new Image();
      image.onload = this.handleLoad.bind(this, 'image_load', null);
      reader.onload = this.handleLoad.bind(this, 'reader_load', image);
      reader.readAsDataURL(file);
    }
  }

  render() {

    return (
      <Container>
        <div className="base-background text-center">
          <img src={this.state.userInfo.head?(ASSETS_HOST+this.state.userInfo.head):require("../../image/user_head.png")} className="head-pic" marginWidth="5"/>
          <div><Button className="btn-radius btn-add-head" onClick={this.handleClick.bind(this,'change_head')}>更改头像</Button></div>
        </div>
        <List className="margin-v list-small-size">
          <List.Item linkComponent={Link} onClick={this.handleClick.bind(this, "nick_edit")} title="昵称" after={this.state.userInfo.name} />
          <List.Item href="#/modifyMobile" title="手机号" after={this.state.userInfo.mobile} />
        </List>
        <List className="margin-v list-small-size">
          <List.Item href="#/setPass" title="重置密码" />
          <List.Item href="#/recommend" title="意见反馈" />
          {
            (this.state.userInfo.type > 0 && this.state.userInfo.type % 2 == 0) ? (
                <List.Item href="#/consults" title="咨询回复" />
              ): null
          }
        </List>
        <div className="base-padding">
          <Button id="logout_btn" amStyle="primary" onClick={this.handleClick.bind(this, "logout_btn")} block>退出登录</Button>
        </div>
        <PromptDialog
          title="修改昵称"
          isOpen={false}>
          <Field placeholder="请输入新的昵称" onChange={this.defaultHandleChange.bind(this,'name_input')}/>
        </PromptDialog>
        <input type="file" onChange={this.handleChange.bind(this)} ref={input => {this.fileInput = input;}} accept="image/jpg,image/jpeg,image/png" className="base-hide" />
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle, showPrompt}, dispatch))(PageSetting);
