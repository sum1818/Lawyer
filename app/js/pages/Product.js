import PageBase from './PageBase';
import React from "react";
import {
  Container,
  Group,
  List,
  Button
} from 'amazeui-touch';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {vipState,buyVip,queryOrder,getUserInfo} from "../apis/UserApi"
import * as XUtil from "../utils/XUtil";
import showToast from "../utils/ToastUtil";

class Product extends PageBase {

  componentWillMount() {
    document.title = "服务";
    let isSelectCard = XUtil.getItem("cardType")?true:false;
    this.state = {vipInfo:{state: -1, vip:{}}, cardType: XUtil.getItem("cardType") || 0, isSelectCard};
    XUtil.removeItem("cardType");//删除CardType
    this.reloadData();
  }

  reloadData() {
    vipState().then(result => {
      if (!result.code) {
        this.setState({vipInfo: result});
      }
    });
  }

  handleClick() {
    if (this.state.isSelectCard) {//选择了会员卡类型
      getUserInfo().then(user => {
        if (user.openid) {
          let callback = this.reloadData.bind(this);
          buyVip(this.state.cardType, 1, user.openid).then(data => {
            if (data.code == 0) {
              if (typeof(WeixinJSBridge) != "undefined") {
                let wxData = {
                  "appId": data.appId,
                  "timeStamp": data.timeStamp+"",
                  "nonceStr":data.nonceStr,
                  "package":data.package,
                  "signType":data.signType,
                  "paySign":data.paySign
                };
                WeixinJSBridge.invoke(
                  'getBrandWCPayRequest', wxData, function(res){
                    if(res.err_msg == "get_brand_wcpay_request:ok" ) {//支付成功
                      queryOrder(data.tradeNo).then(result => {
                        if (result.code == 0) {
                          location.replace("#/buyResult?orderNo="+ data.tradeNo);
                          callback();
                        }
                      });
                    }
                  }
                );
              } else {
                showToast("请在微信客户端内下单购买!");
              }
            } else {
              showToast(data.error);
            }
          });
        } else {
          showToast("尚未绑定您的微信，请重新登录后尝试!");
        }
      })
    } else {//没有选择会员卡类型
      location.replace("#/productList");
    }
  }

  enterPublic() {
    if (this.state.vipInfo.state==1) {
      location.href = "#/attention";
    }
  }

  render() {//
    let vipState = this.state.vipInfo.state;
    let vipInfo = this.state.vipInfo.vip;
    let vipExt;
    let cardNum = vipInfo!=null ? vipInfo.cardNum: null;
    let vipType = this.state.cardType;
    if (vipInfo != null && vipInfo.type != null) {
      vipType = vipInfo.type;
      this.state.cardType = vipType;
    }
    let btnShow = true;
    if (vipState === -1) {//未开通VIP
      vipExt = '您尚未开通服务';
    } else if (vipState === -2) {//VIP过期
      vipExt = '您的服务卡已到期';
    } else if (vipState === 2){//尚未付款
      vipExt = '您的服务申请被拒绝';
    } else if (vipState === 3){//尚未激活
      btnShow = false;
      vipExt = '您的服务卡尚未激活';
    } else if (vipState === 0){//尚未付款
      vipExt = '您的订单尚未完成支付';
    } else {
      btnShow = false;
      vipExt = '有效期至' + XUtil.convertDate(new Date(vipInfo.endtime * 1000), "YYYY-MM-DD");
    }
    let vipClass = ["vip-card", "vip-card-company", "vip-card-group"][vipType] || "vip-card";//
    let buttomButton;
    if (btnShow) {
      buttomButton = (
        <Group
          className="margin-0"
          component="span">
          <Button amStyle="primary" block onClick={this.handleClick.bind(this)}>开通服务</Button>
        </Group>
      )
    }
    return (
      <Container fill direction="column" className="product">
        <div className={vipClass + " flex-column base-card"} onClick={this.enterPublic.bind(this)}>
          <img src={require('../../image/vip-card.jpg')}/>
          <label className={this.state.vipInfo.state==1?"vip-detail":"base-hide"}>进入公众号>></label>
          <label className={"vip-number"}>{cardNum}</label>
          <label className={"vip-ext"}>{vipExt}</label>
        </div>
        <Container scrollable>
          <Group
            header="服务卡特权"
            className="margin-0"
            component="span"
            noPadded
          >
            <List className="margin-0 base-background">
              <List.Item className="white-background margin-sm" title="1、专属法律问题诊断服务（1次/年）"/>
              <List.Item className="white-background margin-sm" title="2、律师专员全年快速响应服务"/>
              <List.Item className="white-background margin-sm" title="3、常规类法律咨询解答"/>
              <List.Item className="white-background margin-sm" title="4、合同起草、审核、修订（3份/年）"/>
              <List.Item className="white-background margin-sm" title="5、代书法律文书，包括但不限于起诉状、上诉状、代理词等（2份/年）"/>
              <List.Item className="white-background margin-sm" title="6、商务或个人法律谈判（2次/年）"/>
              <List.Item className="white-background margin-sm" title="7、委托法律事务收费按浙江省收费标准享受6.6折"/>
              <List.Item className="white-background margin-sm" title="8、最终解释权归浙江纳古律师事务所"/>
            </List>
          </Group>
        </Container>
        {
          buttomButton
        }
      </Container>
    );
  }
}

export default Product;
