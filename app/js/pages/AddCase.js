import PageBase from "./PageBase";
import React from "react";
import {Container, Group, List, Field} from "amazeui-touch";
import {connect} from "react-redux";
import {Link} from "react-router";
import * as XUtil from "../utils/XUtil";
import {bindActionCreators} from "redux";
import {setTitle, setMenu} from "../actions/ActionCreator";
import showToast from "../utils/ToastUtil";
import * as lawyerApi from "../apis/LawyerApi";
import {partyTypes} from '../const';

class AddCase extends PageBase {

  constructor() {
    super();
    this.state = {party:[], session:[], material:[],conflicts:[],conflictGo:false};
  }

  /**
   * 校验案件数据
   */
  validCaseData() {
    let data = null;
    //数据校验
    if (!this.state.guest) {
      showToast("请选择委托人");
    } else if (!this.state.type){
      showToast("请选择案件类型")
    } else if (!this.state.visible) {
      showToast("请选择可查看人");
    } else if (XUtil.isEmpty(this.state['year_input'])) {
      showToast("请输入年份");
    } else if (XUtil.isEmpty(this.state['word_input'])) {
      showToast("请输入代字");
    } else if (XUtil.isEmpty(this.state['num_input'])) {
      showToast("请输入案号");
    } else if (XUtil.isEmpty(this.state['reason_input'])) {
      showToast("请输入案由");
    } else {
      data = {};
      data.casenum = "(" + this.state['year_input'] + ")" + this.state['word_input'] + "字第" + this.state['num_input'] + "号";//案号
      data.guestid = this.state.guest.id;//委托人
      data.reason = this.state['reason_input'];//案由
      let perties = [];
      let delegates = [];
      this.state.party.forEach(party => {
        let partyArr = party.split("|");
        let selected = partyArr.length > 4 ? Boolean(parseInt(partyArr[4])) : false;
        party = `${partyArr[0]}|${partyArr[1]}|${partyArr[2]}`;
        perties.push(party);
        selected && delegates.push(party);
      });
      data.parties = perties.join(";");//当事人
      data.delegates = delegates.join(";");//我方代理人
      data.looklevel = this.state.visible.index;//可查看人
      data.news = this.state['news_input'];//案件动态
      data.sessions = this.state.session.join(";");//开庭信息
      data.materials = this.state.material.join(";");//材料信息
      data.type = this.state.type.index;//案件类型
      data.state = this.state.state?this.state.state.index:0;//案件状态
      data.remark = this.state['remark_input']||"";//案件备注
    }
    return data;
  }

  componentWillMount() {
    this.props.setTitle("添加案件");
    this.props.setMenu("保存", null, ()=>{
      let data = this.validCaseData();//数据校验
      data && this.validCaseConflict().then(isConflict => {
        if (isConflict && !this.state.conflictGo) {
          this.state.conflictGo = true;
          showToast("本案可能存在冲突, 如确定不存在冲突, 请继续点击保存");
        } else {//没有冲突直接保存
          data && lawyerApi.addCase(data)
            .then(data=>{
              if (data.code == 0) {
                showToast(data.result);
                XUtil.removeItem("editCase");
                history.back();
              }
            });
        }
      });
    });
    //通过session获取之前保存的信息
    Object.assign(this.state, JSON.parse(XUtil.getItem("editCase")));
    if (XUtil.isEmpty(this.state['year_input'])) {
      this.setState({'year_input':String(new Date().getFullYear())});
    }
  }

  validCaseConflict() {
    const checkFields = ['year_input', 'word_input' ,'num_input'];
    for (let i in checkFields) {
      if (checkFields.hasOwnProperty(i)) {
        let field = checkFields[i];
        let num = this.state[field];
        if (XUtil.isEmpty(num)) {
          return;
        }
      }
    }
    let casenum = "(" + this.state['year_input'] + ")" + this.state['word_input'] + "字第" + this.state['num_input'] + "号";//案号
    return lawyerApi.conflickCheck(this.caseId || 0,casenum).then(data => {
      this.setState({conflicts:data.list});
      return data.list.length > 0;
    });
  }

  handleBlur(comId) {
    let editCase = JSON.parse(XUtil.getItem("editCase"))||{};
    editCase[comId] = this.state[comId];
    XUtil.setItem("editCase", JSON.stringify(editCase));
    if (comId == 'year_input' || comId == 'word_input' || comId == 'num_input') {
      this.state.conflictGo = false;
      this.validCaseConflict();
    }
  }

  render() {
    const guest = this.state.guest;
    const type = this.state.type;
    const parties = this.state.party;
    const sessions = this.state.session;
    const materials = this.state.material;
    const visible = this.state.visible;
    const state = this.state.state;
    const finishState = this.state.finishState;
    let delegates = [];
    if (parties != null && parties.length > 0) {
      parties.forEach(party => {
        let partyArr = party.split("|");
        let selected = partyArr.length > 4 ? Boolean(parseInt(partyArr[4])) : false;
        selected && delegates.push(partyArr[0]);
      });
    }
    return (
      <Container scrollable>
        <List className="margin-top-sm margin-bottom-0 list-small-size">
          <List.Item linkComponent={Link} linkProps={{to: {pathname: "/pageCustomer", state: {from: "addCase"}}}} title="委托人" after={guest?(guest.remark || guest.name|| " "):" "}/>
          <List.Item linkComponent={Link} linkProps={{to: {pathname: "/caseType", state: {from: "addCase", index: type?type.index:0}}}} title="案件类型" after={type?(type.title):" "}/>
          <List.Item linkComponent={Link} linkProps={{to: {pathname: "/visibleType", state: {from: "addCase", index: visible?visible.index:0}}}} title="可查看人" after={visible?(visible.title):" "}/>
          <List.Item linkComponent={!finishState?Link:null} linkProps={{to: {pathname: "/stateType", state: {from: "addCase", index: state?state.index:0}}}} title="当前阶段" after={state?(state.title):" "}/>
          <List.Item nested="input" title="案号">
            <div className="case-num-div">
              <div className="year-div">
                (
                  <input type="number"
                         onChange={this.defaultHandleChange.bind(this, 'year_input')}
                         onBlur={this.handleBlur.bind(this, 'year_input')}
                         value={this.state['year_input']||""}/>
                )
              </div>
              <div className="word-div">
                &nbsp;&nbsp;
                <input type="text" placeholder="请输入代字"
                       onChange={this.defaultHandleChange.bind(this, 'word_input')}
                       onBlur={this.handleBlur.bind(this, 'word_input')}
                       value={this.state['word_input']||""}/>
                字&nbsp;&nbsp;
              </div>
              <div className="num-div">
                第
                <input type="number"
                       onChange={this.defaultHandleChange.bind(this, "num_input")}
                       onBlur={this.handleBlur.bind(this, "num_input")}
                       value={this.state['num_input']||""}/>
                号
              </div>
            </div>
          </List.Item>
          <List.Item className={this.state.conflicts.length > 0?'':"base-hide"}>
            <div className="main-color md-size">
              <div>本案与下列案件可能存在利益冲突，请仔细核对:</div>
              {
                this.state.conflicts.map(conflict=>{
                  let delegates = conflict.delegates.split(";");
                  let delegateStr = "我方代理：";
                  delegateStr += delegates.map(delegate => {
                    let type = delegate.substring(delegate.lastIndexOf("|")+1,delegate.length);
                    return partyTypes[parseInt(type)] + "-" + delegate.substring(0, delegate.indexOf("|"))
                  }).join(",");
                  return (
                    <div>{conflict.casenum} {delegateStr}</div>
                  )
                })
              }
            </div>
          </List.Item>
          <List.Item nested="input">
            <Field
              containerClassName="no-arrow"
              type="textarea"
              placeholder="请输入案由"
              onChange={this.defaultHandleChange.bind(this, "reason_input")}
              onBlur={this.handleBlur.bind(this, "reason_input")}
              value={this.state['reason_input']||""}
            />
          </List.Item>
          <List.Item nested="input" title="案件动态">
            <Field
              className="no-arrow flex-3 text-right"
              placeholder="请输入案件动态信息"
              onChange={this.defaultHandleChange.bind(this, "news_input")}
              onBlur={this.handleBlur.bind(this, "news_input")}
              value={this.state['news_input']||""}
            />
          </List.Item>
        </List>
        <List className="margin-v-sm list-small-size">
          {
            parties.map((party, i) => {
              let partyArr = party.split("|");
              return (<List.Item key={`party${i}`} title={partyArr[3]} after={partyArr[0]} linkComponent={Link} linkProps={{to: {pathname:"/addParty", state: {party, from:"addCase"}}}}/>);
            })
          }
        </List>
        <div className="base-background base-center padding-v-xs text-primary">
          <Link to={{pathname: "/addParty"}}>
          + 添加当事人
          </Link>
        </div>
        <List className="margin-v-sm list-small-size">
          <List.Item linkComponent={Link} linkProps={{to: {pathname: "/delegateUser", state: {from: "addCase", index: visible?visible.index:0}}}} title="我方代理" after={delegates.join(",")}/>
        </List>
        <List className="margin-v-sm list-small-size">
          {
            sessions.map((session, i) => {
              let sessionArr = session.split("|");
              return (<List.Item key={`session${i}`} title={sessionArr[0]+"开庭"} after={XUtil.convertDate(new Date(parseInt(sessionArr[2])), 'YYYY-MM-DD hh:mm')} linkComponent={Link} linkProps={{to: {pathname:"/addSession", state: {session, from:"addCase"}}}}/>);
            })
          }
        </List>
        <div className="base-background base-center padding-v-xs text-primary">
          <Link to={{pathname: "/addSession"}}>
            + 添加开庭信息
          </Link>
        </div>
        <List className="margin-v-sm list-small-size">
          {
            materials.map((material, i) => {
              let materialArr = material.split("|");
              return (<List.Item key={`material${i}`} title={materialArr[0]} after={materialArr[2]} linkComponent={Link} linkProps={{to: {pathname:"/addMaterial", state: {material, from:"addCase"}}}}/>);
            })
          }
        </List>
        <List className="margin-v-sm list-small-size">
          <List.Item nested="input">
            <Field
              containerClassName="no-arrow"
              type="textarea"
              placeholder="请输入备注信息"
              onChange={this.defaultHandleChange.bind(this, "remark_input")}
              onBlur={this.handleBlur.bind(this, "remark_input")}
              value={this.state['remark_input']||""}
            />
          </List.Item>
        </List>
      </Container>
    );
  }
}

export {AddCase as AddCaseOrg};

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu}, dispatch))(AddCase);
