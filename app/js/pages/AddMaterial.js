import PageBase from "./PageBase";
import React from "react";
import {Container, Group, List, Field, Grid, Col} from "amazeui-touch";
import {connect} from "react-redux";
import {Link} from "react-router";
import {bindActionCreators} from "redux";
import {setTitle, setMenu} from "../actions/ActionCreator";
import * as XUtil from "../utils/XUtil";
import showToast from "../utils/ToastUtil";
import {DateTimePicker} from "../widgets/datatimepicker/index";

class AddMaterial extends PageBase {

  constructor() {
    super();
    this.state = {
      time: new Date(),
      isOpen: false
    };
  }

  componentWillMount() {
    this.props.setTitle("案件材料");
    this.editCase = JSON.parse(XUtil.getItem("editCase")) || {};
    this.editMaterial = 0;
    if (!this.editCase.material) {
      this.editCase.material = [];
    }
    if (this.props.location.state != null) {
      this.state.from = this.props.location.state.from;
      let material = this.props.location.state.material;
      let materialArr = material.split("|")
      this.state.type = {};//阶段
      this.state.type['title'] = materialArr[0];
      this.state.type['index'] = parseInt(materialArr[1]);
      this.state['type_input'] = materialArr[2];
      this.state.time = new Date(parseInt(materialArr[3]));
      this.state.title = XUtil.convertDate(this.state.time, 'YYYY-MM-DD hh:mm');
      this.state['remark_input'] = materialArr[4];
      //确定编辑的位置
      for(let i =0; i< this.editCase.material.length;i++) {
        let str = this.editCase.material[i];
        if (str == material) {
          this.editMaterial = i;
        }
      }
    }
    this.props.setMenu("保存", null, ()=>{
      if (!this.state.type) {
        showToast("请选择阶段");
      } else if (XUtil.isEmpty(this.state['type_input'])) {
        showToast("材料类型不能为空");
      } else if (!this.state.title) {
        showToast("请选择签收时间");
      } else {
        let remark = this.state['remark_input']||'';
        let material = `${this.state.type.title}|${this.state.type.index}|${this.state['type_input']}|${this.state.time.valueOf()}|${remark}`;
        if (this.state.from == 'addCase') {
          this.editCase.material[this.editMaterial] = material;
        } else {
          this.editCase.material.push(material);
        }
        XUtil.setItem("editCase", JSON.stringify(this.editCase));
        history.back();
      }
    });
    let type = XUtil.getItem("selectMaterialType");
    if (type != null) {
      XUtil.removeItem("selectMaterialType");
      this.setState({type: JSON.parse(type)});
    }
  }

  handleClick(comId) {
    switch (comId) {
      case 'material_time':
        this.setState({ isOpen: true });
        break;
    }
  }

  handleCancel() {
    this.setState({ isOpen: false });
  }

  handleSelect(time) {
    this.setState({ time, isOpen: false, title: XUtil.convertDate(time, 'YYYY-MM-DD hh:mm')});
  }

  render() {
    const type = this.state.type;
    const title = this.state.title;
    return (
      <Container>
        <List className="margin-0 list-small-size">
          <List.Item linkComponent={Link} linkProps={{to: {pathname: "/materialType", state: {from: "addMaterial", index: type?type.index:0}}}} title={type?(type.title):"请选择阶段"} />
          <List.Item nested="input">
            <Field
              containerClassName="no-arrow"
              placeholder="请输入材料类型"
              onChange={this.defaultHandleChange.bind(this, "type_input")}
              value={this.state['type_input']||""}
            />
          </List.Item>
          <List.Item linkComponent={Link} onClick={this.handleClick.bind(this, 'material_time')} title={title||"请选择签收时间"} />
          <List.Item nested="input">
            <Field
              containerClassName="no-arrow"
              type="textarea"
              placeholder="请输入备注信息"
              onChange={this.defaultHandleChange.bind(this, "remark_input")}
              value={this.state['remark_input']||""}
            />
          </List.Item>
          <List.Item className="base-background" title="上传材料附件"/>
        </List>
        <Grid avg={4} className="sk-icons text-center">
          <Col shrink={true} className="padding-top-sm padding-bottom-0 padding-h-xs">
            <a className="pic-take-btn base-icon-add"/>
          </Col>
        </Grid>
        <DateTimePicker
          value={this.state.time}
          isOpen={this.state.isOpen}
          theme="ios"
          onSelect={this.handleSelect.bind(this)}
          onCancel={this.handleCancel.bind(this)} />
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu}, dispatch))(AddMaterial);
