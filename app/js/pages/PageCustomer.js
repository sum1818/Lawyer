import PageBase from './PageBase';
import React from "react";
import {Container, Group, List, Field, Button, Icon} from "amazeui-touch";
import {connect} from "react-redux";
import { Link } from 'react-router';
import {bindActionCreators} from "redux";
import {setTitle, setMenu} from "../actions/ActionCreator";
import * as pinYin from "../utils/PinYin";
import * as lawyerApi from "../apis/LawyerApi";
import showToast from "../utils/ToastUtil";
import SearchInput from "../widgets/SearchInput";
import {ASSETS_HOST} from '../const';
import * as XUtil from "../utils/XUtil";

class PageCustomer extends PageBase {

  static contextTypes = {//注入router对象
    router: React.PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.state = {
      guests: [],
      letters: new Set()
    }
  }

  componentWillMount() {
    this.props.setTitle("客户管理");
    this.props.setMenu(null, "customer-white", '#/addCustomer');
    if (this.props.location.state != null) {
      this.state.from = this.props.location.state.from;
    }
    lawyerApi.getGuestList()
      .then(data => {
        if (data.code == 0) {
          data.list.forEach(guest => {
            guest.letter = pinYin.getFirstLetter(guest.remark || guest.name) || "#";//获取首字母
            guest.letter = guest.letter.substring(0, 1).toUpperCase();
          });
          this.setState({guests: data.list});
        } else {
          showToast(data.error);
        }
      });
  }

  handleClick(comId, guest) {
    switch (comId) {
      case 'guest_item':
        if (this.state.from == 'addCase') {//如果是添加案件页面转向的
          let editCase = Object.assign({},JSON.parse(XUtil.getItem("editCase")),{guest});
          XUtil.setItem("editCase", JSON.stringify(editCase));//记录选中的客户
          history.back();
        } else {
          this.context.router.push({pathname:'/customerDetail',state: guest});
        }
        break;
      case 'letter_item':
        console.log(guest);
        let customerList = document.getElementById("customer_list");
        let letterItem = document.getElementById("letter" + guest);
        if (letterItem) {
          customerList.scrollTop = letterItem.offsetTop - customerList.offsetTop;
        }
        break;
    }
  }

  searchHandle() {
    if (this.state['btn_search']) {
      this.context.router.push({pathname:'/searchCustomer',state: {name:this.state['btn_search']}});
    } else {
      showToast("请输入昵称或手机号");
    }
  }

  render() {
    const items = [];
    //先排下序
    this.state.guests.sort((item1, item2) => {
      if (item1.letter == '#') return 1;
      if (item2.letter == '#') return -1;
      return item1.letter.charCodeAt(0) - item2.letter.charCodeAt(0);
    });
    this.state.letters.clear();
    this.state.guests.forEach(guest => {
      let letter = guest.letter;
      if (!this.state.letters.has(letter)) {
        this.state.letters.add(letter);
        items.push((<List.Item id={"letter"+letter} key={letter} role="header">{letter}</List.Item>));
      }
      items.push((<List.Item key={guest.id} media={<img className="img-double-size img-oval" src={guest.head?(ASSETS_HOST+guest.head):require("../../image/user_head.png")} />} title={guest.remark || guest.name || " "} linkComponent={Link} linkProps={{className:"padding-v-xs item-hide-right-ico", onClick: this.handleClick.bind(this, 'guest_item', guest)}}/>));
    });

    const letters = [];
    for (let i = 'A'.charCodeAt(0);i <= 'Z'.charCodeAt(0); i++) {
      letters.push(String.fromCharCode(i));
    }
    letters.push("#");
    return (
      <div className="flex-column">
        <SearchInput
          placeholder="搜索客户"
          onSubmit={this.searchHandle.bind(this)}
          onChange={this.defaultHandleChange.bind(this, 'btn_search')}
        />
        <Container id="customer_list" scrollable>
          <List className="margin-0 list-small-size">
            {items}
          </List>
        </Container>
        <div className="letter-list">
          {
            letters.map(letter => {
              return <div key={`letter${letter}`} onClick={this.handleClick.bind(this, 'letter_item', letter)}>{letter}</div>;
            })
          }
        </div>
      </div>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu}, dispatch))(PageCustomer);
