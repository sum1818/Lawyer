import PageBase from './PageBase';
import React from "react";
import {
  Container,
  Group,
} from 'amazeui-touch';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle} from "../actions/ActionCreator";

class About extends PageBase {

  componentWillMount() {
    this.props.setTitle("关于我们");
  }
  render() {
    return (
      <Container>
        <div className="base-center margin-top-lg">
            <img src={require("../../image/logo.png")} className="head-pic"/>
            <div>v1.0.0</div>
        </div>
        <div className="foot-container sm-size">
          纳古律师@Copyright 2016-2020 ZheJiang NaGu
        </div>
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle}, dispatch))(About);
