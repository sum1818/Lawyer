import React from "react";
import {Container, Group, List, Field, Button, Icon, View} from "amazeui-touch";
import {connect} from "react-redux";
import {Link} from "react-router";
import {bindActionCreators} from "redux";
import {setTitle, setMenu} from "../actions/ActionCreator";
import * as lawyerApi from "../apis/LawyerApi";
import showToast from "../utils/ToastUtil";
import SearchNav from "../widgets/SearchNav";
import {InputStateChangeHandler} from "../handler/InputChangeHandler";

class SearchCase extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      cases: []
    }
    this.state['customer_search'] = props.location.state.name;
  }

  componentWillMount() {
    this.searchCase();
    this.props.setTitle("搜索");
  }

  searchCase() {
    this.state.cases = [];
    lawyerApi.searchCase(this.state['customer_search'])
      .then(data => {
        if (data.code == 0) {
          this.setState({cases: data.list});
        } else {
          showToast(data.error);
        }
      });
  }

  handleClick(comId) {
    switch (comId) {
      case 'cancel_btn':
        history.back();
        break;
      case 'search_btn':
        this.searchCase();
        break;
    }
  }

  render() {
    const items = [];
    this.state.cases.forEach(item => {
      items.push((<List.Item key={item.id} title={(item.casenum || " ") + " (" + item.reason + ")"} linkComponent={Link} linkProps={{to: {pathname: "/caseDetail", state: {id: item.id}}}}/>));
    });

    return (
      <View>
        <SearchNav
          onSubmit={this.handleClick.bind(this, 'search_btn')}
          onChange={InputStateChangeHandler.bind(this, 'customer_search')}
          onCancel={this.handleClick.bind(this, 'cancel_btn')}
          placeholder="请输入案号或案由..."
          value={this.state['customer_search']||""}
        />
        <Container scrollable>
          <List className="margin-0 list-small-size">
            {items}
          </List>
        </Container>
      </View>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu}, dispatch))(SearchCase);
