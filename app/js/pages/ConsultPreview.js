/**
 * Created by zl on 2017/10/30.
 */
import PageBase from './PageBase';
import React from "react";
import {
  Container,
  Group,
} from 'amazeui-touch';
import {ASSETS_HOST} from "../const";

class ConsultPreview extends PageBase {

  constructor(props) {
    super(props);
    let attachment = props.location.state.attachment.split(',');
    this.state = {attachment};
  }

  componentWillMount() {
    document.title = "咨询";
  }
  render() {
    return (
      <Container scrollable>
        {
          this.state.attachment.map((item,index) => {
            return (<img key={'img_'+index} src={ASSETS_HOST + item}/>);
          })
        }
      </Container>
    );
  }
}

export default ConsultPreview;
