import PageBase from './PageBase';
import React from "react";
import {
  Container,
  Group,
} from 'amazeui-touch';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {vipState} from "../apis/UserApi";
import * as XUtil from "../utils/XUtil";

class ProductList extends PageBase {

  componentWillMount() {
    document.title = "服务";
  }

  handleClick(comId) {
    vipState().then(result => {
      if (result.state != 1) {//非会员用户
        if (comId == 'person_card') {
          XUtil.setItem("cardType", 0);
        } else if (comId == 'group_card') {
          XUtil.setItem("cardType", 2);
        } else if (comId == 'company_card') {
          XUtil.setItem("cardType", 1);
        }
      }
      location.href = "#/product";
    });
  }

  render() {
    return (
      <Container fill scrollable className="product">
        <div className="vip-card base-card flex-column margin-bottom-0" onClick={this.handleClick.bind(this, 'person_card')}>
          <img src={require('../../image/vip-card.jpg')}/>
          <label className={"vip-fee"}>资费标准: 1980元/年，活动价880元/年</label>
          <label className="vip-type">
            普惠
          </label>
          <label className="vip-detail">
            查看服务详情>>
          </label>
        </div>
      </Container>
    );
  }
}

export default ProductList;
