import React from 'react';
import {
  Container,
  List,
  NavBar,
  View,
  Badge,
  Link
} from 'amazeui-touch';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {saveCache,delCache} from "../actions/ActionCreator";
import SearchNav from "../widgets/SearchNav";
import {InputStateChangeHandler} from "../handler/InputChangeHandler";
import * as XUtil from "../utils/XUtil";
import * as publicApi from "../apis/PublicApi";
import {ASSETS_HOST} from '../const';

class SearchLawyer extends React.Component {

  static contextTypes = {//注入router对象
    router: React.PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {};
    if (props.location.action == "PUSH") {
      props.delCache(['search_input','searchLawyers']);
    } else {
      this.state['search_input'] = props['search_input']||"";
    }
  }

  componentWillMount() {
    document.title = "搜索";
  }

  handleClick(comId, item, event) {
    switch (comId) {
      case 'cancel_btn':
        this.context.router.goBack();
        break;
      case 'search_btn':
        if (!XUtil.isEmpty(this.state['search_input'])) {
          publicApi.queryLawyers(this.state['search_input'])
            .then(data => {
              if (data.code == 0 && data.list) {
                this.props.saveCache({searchLawyers: data.list, search_input: this.state['search_input']});
              }
            });
        }
        break;
      case 'item_click':
        this.context.router.push({pathname:'/lawyerDetail',state: {id: item}});
        break;
      case 'lawyer-chat':
        this.context.router.push({pathname:'/messageDetail', state: {referId:item.id, utype:1,name: item.name}});
        break;
    }
    event.stopPropagation();
  }

  render() {
    const lawyers = this.props.searchLawyers || [];
    return (
      <View>
        <SearchNav
          onSubmit={this.handleClick.bind(this, 'search_btn')}
          onChange={InputStateChangeHandler.bind(this, 'search_input')}
          onCancel={this.handleClick.bind(this, 'cancel_btn')}
          value={this.state['search_input']}
          placeholder="请输入律师姓名..."
        />
        <Container scrollable>
          <List className="margin-0 index_lawyer_list base-background">
            {
              lawyers.map((lawyer,i) => {
                return(
                  <List.Item key={"lawyer" + i} onClick={this.handleClick.bind(this, 'item_click', lawyer.id)} className="padding-0 margin-h-xsm margin-top-xsm white-background">
                    <div className="item-main padding-h-xs padding-v-sm">
                      <div><img src={lawyer.head?(ASSETS_HOST+lawyer.head):require("../../image/news.png")}/></div>
                      <div className="block-div content">
                        <div className="item-main">
                          <div className="title flex-1">{lawyer.name}</div>
                          <div className="consult-btn" onClick={this.handleClick.bind(this, 'lawyer-chat', lawyer)}>咨询TA</div>
                        </div>
                        <div><span>执业:</span> {lawyer.years}年</div>
                        <div><span>地域: </span>{lawyer.area}</div>
                        <div><span>专长: </span>{lawyer.specialty}</div>
                      </div>
                    </div>
                  </List.Item>
                )
              })
            }
          </List>
        </Container>
      </View>
    );
  }
}

export default connect(state => state.SaveCache, dispatch => bindActionCreators({saveCache, delCache}, dispatch))(SearchLawyer);
