import React from 'react';
import {
  Container,
  Group,
} from 'amazeui-touch';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle} from "../actions/ActionCreator";
import PageBase from './PageBase';
import * as informationApi from '../apis/InformationApi';

class NewsDetail extends PageBase {

  constructor(props) {
    super(props);
    this.state = {url: props.location.state.url, content: ""};
  }

  componentWillMount() {
    this.props.setTitle("新闻资讯");
    informationApi.getUrlContent(this.state.url).then(data => {
      this.setState({content: data});
    });
  }

  render() {
    return (
      <Container fill scrollable className="white-background" {...this.props} dangerouslySetInnerHTML={{__html: this.state.content}}/>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle}, dispatch))(NewsDetail);
