import PageBase from './PageBase';
import React from "react";
import {
  Container,
  Group,
  Button
} from 'amazeui-touch';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle} from "../actions/ActionCreator";
import * as informationApi from '../apis/InformationApi';
import * as XUtil from '../utils/XUtil';

class Protocol extends PageBase {

  componentWillMount() {
    this.props.setTitle("协议");
    this.state = {};
    informationApi.getUrlContent("/protocol/register.html").then(data => {
      this.setState({content: data});//获取注册协议
    });
  }

  handleClick(comId) {
    if (comId == 'reject_btn') {
      let key = "protocol_" + XUtil.getItem("mobile");
      XUtil.removeItem(key);
    }
    history.back();
  }

  render() {//
    return (
      <Container fill direction="column">
        <Container scrollable className="white-background flex-1 padding-h-xs" {...this.props} dangerouslySetInnerHTML={{__html: this.state.content}}/>
        <div className="white-background flex-row padding-h">
          <Button amStyle="success" className="flex-1 margin-h" block onClick={this.handleClick.bind(this,'agree_btn')}>同意</Button>
          <Button amStyle="primary" className="flex-1 margin-h" block onClick={this.handleClick.bind(this,'reject_btn')}>拒绝</Button>
        </div>
      </Container>

    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle}, dispatch))(Protocol);
