import PageBase from "./PageBase";
import React from "react";
import {Container, Group, List, Field} from "amazeui-touch";
import {connect} from "react-redux";
import { Link } from 'react-router';
import {bindActionCreators} from "redux";
import {setTitle, setMenu} from "../actions/ActionCreator";
import * as XUtil from "../utils/XUtil";
import showToast from "../utils/ToastUtil";

class AddParty extends PageBase {

  constructor() {
    super();
    this.state = {};
  }

  componentWillMount() {
    this.props.setTitle("当事人");
    this.editCase = JSON.parse(XUtil.getItem("editCase")) || {};
    this.editParty = 0;
    if (!this.editCase.party) {
      this.editCase.party = [];
    }
    if (this.props.location.state != null) {
      this.state.from = this.props.location.state.from;
      let party = this.props.location.state.party;
      let partyArr = party.split("|");
      this.state['name_input'] = partyArr[0];
      this.state['mobile_input'] = partyArr[1];
      this.state.type = {};
      this.state.type['index'] = parseInt(partyArr[2]);
      this.state.type['title'] = partyArr[3];
      //确定编辑的位置
      for(let i =0; i< this.editCase.party.length;i++) {
        let str = this.editCase.party[i];
        if (str == party) {
          this.editParty = i;
        }
      }
    }
    this.props.setMenu("保存", null, ()=>{
      if (!this.state.type) {
        showToast("请选择当事人身份");
      } else if (XUtil.isEmpty(this.state['name_input'])) {
        showToast("当事人名称不能为空");
      } else if (!XUtil.isMobile(this.state['mobile_input'])) {
        showToast("手机号码格式不正确");
      } else {
        let party = `${this.state['name_input']}|${this.state['mobile_input']}|${this.state.type.index}|${this.state.type.title}`;
        if (this.state.from == 'addCase') {
          let orgPartyArr = this.editCase.party[this.editParty].split("|");
          if (orgPartyArr.length > 4) {
            party += `|${orgPartyArr[4]}`;
          }
          this.editCase.party[this.editParty] = party;
        } else {
          this.editCase.party.push(party);
        }
        XUtil.setItem("editCase", JSON.stringify(this.editCase));
        history.back();
      }
    });
    let type = XUtil.getItem("selectPartyType");
    if (type != null) {
      XUtil.removeItem("selectPartyType");
      this.setState({type: JSON.parse(type)});
    }
  }

  render() {
    const type = this.state.type;
    return (
      <Container>
        <List className="margin-0 list-small-size">
          <List.Item key="select_type" linkComponent={Link} linkProps={{to: {pathname: "/partyType", state: {from: "addParty", index: type?type.index:0}}}} title={type?(type.title):"请选择当事人身份"} />
          <List.Item key="name_input" nested="input">
            <Field
              containerClassName="no-arrow"
              placeholder="请输入当事人名称"
              onChange={this.defaultHandleChange.bind(this, "name_input")}
              defaultValue={this.state['name_input']}
            />
          </List.Item>
          <List.Item key="mobile_input" nested="input">
            <Field
              containerClassName="no-arrow"
              placeholder="请输入当事人手机号码"
              onChange={this.defaultHandleChange.bind(this, "mobile_input")}
              defaultValue={this.state['mobile_input']}
            />
          </List.Item>
        </List>
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu}, dispatch))(AddParty);
