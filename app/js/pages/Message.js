import React from "react";
import {Container, List, NavBar, View, Badge} from "amazeui-touch";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {initIM} from "../actions/ActionCreator";
import * as messageApi from "../apis/MessageApi";
import * as XUtil from "../utils/XUtil";
import {ASSETS_HOST} from '../const';

class Message extends React.Component {

  static contextTypes = {//注入router对象
    router: React.PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {chats: [], unread:{}};
  }

  componentWillMount() {
    document.title = "消息";
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.count != this.props.count) {
      let unread = this.state.unread;
      unread[nextProps.referId] = unread[nextProps.referId] + 1;
      this.state.chats.forEach(chat => {
        if (chat.referId == nextProps.referId) {
          chat.message = nextProps.msg.message;
          chat.sentTime = nextProps.msg.sentTime;
        }
      });
      this.setState({unread});
    }
  }

  handleClick(chat) {
    this.context.router.push({pathname:'/messageDetail',state: {referId: chat.referId,utype:chat.utype, name: chat.remark||chat.name}});
    let unreadCountTmp = XUtil.getItem("unreadCountTmp", 0);//读取临时值
    unreadCountTmp = parseInt(unreadCountTmp) - parseInt(this.state.unread[chat.referId] || 0);
    XUtil.setItem("unreadCountTmp", 0);
  }

  render() {
    return (
      <View>
        <NavBar
          title="消息"
          amStyle="dark"
        />
        <Container className="white-background" scrollable>
          <List className="conversation-list">
            {
              this.state.chats.map((chat, index) => {
                return (
                  <List.Item key={chat.id} className="padding-0 item-main" onClick={this.handleClick.bind(this, chat)}>
                    <div><img className="conversation-head margin-v-xs margin-h-sm" src={chat.head?(ASSETS_HOST+chat.head):require("../../image/user_head.png")}/></div>
                    <div className="block-div flex-1 padding-right">
                      <div className="flex-row match-parent-width">
                        <div className="md-size flex-1 title base-over">{chat.remark||chat.name}</div>
                        <div className="sm-size middle-color">{XUtil.messageDate(chat.sentTime)}</div>
                      </div>
                      <div className="flex-row match-parent-width">
                        <div className="flex-1 middle-color content base-over">{chat.message}</div>
                        {
                          this.state.unread[chat.referId] > 0?(<Badge amStyle="alert" rounded>{this.state.unread[chat.referId]}</Badge>):null
                        }
                      </div>
                    </div>
                  </List.Item>
                )
              })
            }
          </List>
        </Container>
      </View>
    );
  }
}
export default connect(state => state.UnRead, dispatch => bindActionCreators({initIM}, dispatch))(Message);
