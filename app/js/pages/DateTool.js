import PageBase from './PageBase';
import React from "react";
import {Link} from "react-router";
import {
  Container,
  Group,
  List,
  Button,
  Field
} from 'amazeui-touch';
import * as XUtil from "../utils/XUtil";
import showToast from "../utils/ToastUtil";
import DatePicker from "../widgets/datatimepicker/index";

export default class DateTool extends PageBase {

  componentWillMount() {
    document.title = '日期计算器';
    let currentTime = new Date();
    this.state = {startDate: '', endDate: '', isOpen: false, time: currentTime, starttime: null, endtime: null, flag: 0, result:''};
  }

  handleClick(comId) {
    if (comId == 'start') {
      let starttime = this.state.starttime || new Date();
      this.setState({ isOpen: true, time: starttime, flag: 0 });
    } else if (comId == 'end') {
      let endtime = this.state.endtime || new Date();
      this.setState({ isOpen: true, time: endtime, flag: 1 });
    } else if (comId == 'submit') {
      if (this.state.starttime && this.state.endtime) {//两个时间
        let timeDiff = parseInt(this.state.endtime.valueOf() / 24 / 3600/ 1000) - parseInt(this.state.starttime.valueOf() / 24 /3600/1000);
        if (timeDiff < 0) {
          showToast('结束日期必须大于开始日期。');
        } else {
          this.setState({result: timeDiff + ''});
        }
      } else if (this.state.result.length > 4) {
        showToast('天数最多只能有4位数字。');
      } else if (this.state.starttime && this.state.result) {
        let endtime = new Date(this.state.starttime.valueOf() + parseInt(this.state.result) * 24 * 3600 * 1000);
        this.setState({endtime, endDate: XUtil.convertDate(endtime, 'YYYY-MM-DD')});
      } else if (this.state.endtime && this.state.result) {
        let starttime = new Date(this.state.endtime.valueOf() - parseInt(this.state.result) * 24 * 3600 * 1000);
        this.setState({starttime, startDate: XUtil.convertDate(starttime, 'YYYY-MM-DD')});
      }
    } else {
      this.setState({startDate: '', endDate: '', starttime: null, endtime: null, result:''});
    }
  }

  handleChange(event) {
    let result = event.target.value;
    this.setState({result});
  }

  handleCancel() {
    this.setState({ isOpen: false });
  }

  handleSelect(time) {
    if (this.state.flag == 0) {
      this.setState({ time, starttime: time, isOpen: false, startDate: XUtil.convertDate(time, 'YYYY-MM-DD')});
    } else {
      this.setState({ time, endtime: time, isOpen: false, endDate: XUtil.convertDate(time, 'YYYY-MM-DD')});
    }
  }

  render() {//
    let count = 0;
    if (this.state.starttime) count++;
    if (this.state.endtime) count++;
    if (XUtil.isNumber(this.state.result)) count++;
    let checkForm = count == 2;
    return (
      <Container scrollable className="white-background">
        <div className="margin">提示：选择开始日期、结束日期或时间差的任意两个即可获得另一个计算结果。日期计算不包含起始日期的当日。</div>
        <List className="margin-0 list-small-size">
          <List.Item linkComponent={Link} onClick={this.handleClick.bind(this, 'start')} title={this.state.startDate||"请选择开始时间"} />
          <List.Item linkComponent={Link} onClick={this.handleClick.bind(this, 'end')} title={this.state.endDate||"请选择结束时间"} />
        </List>
        <Group className="margin-xs">
          <Field
            labelBefore="日期差:"
            labelAfter="天"
            type="number"
            placeholder="请输入日期差"
            onChange={this.handleChange.bind(this)}
            value={this.state.result}
          />
        </Group>
        <Group
          className="margin-0"
          component="span">
          <Button amStyle="success" block onClick={this.handleClick.bind(this,'clear')}>清空</Button>
          <Button amStyle="primary" block disabled={!checkForm} onClick={this.handleClick.bind(this,'submit')}>开始计算</Button>
        </Group>
        <DatePicker
          value={this.state.time}
          isOpen={this.state.isOpen}
          theme="ios"
          onSelect={this.handleSelect.bind(this)}
          onCancel={this.handleCancel.bind(this)} />
      </Container>
    );
  }
}

