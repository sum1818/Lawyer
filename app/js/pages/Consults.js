import PageBase from './PageBase';
import React from "react";
import {
  Container,
  Group,
  List,
  Field
} from 'amazeui-touch';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle,showPrompt} from "../actions/ActionCreator";
import {getConsults, putReply} from "../apis/AdminApi"
import * as XUtil from "../utils/XUtil"
import showToast from "../utils/ToastUtil";
import PromptDialog from "../dialogs/PromptDialog";

class Consults extends PageBase {

  componentWillMount() {
    this.props.setTitle("咨询回复");
    this.state = {consults:[], currentPage: 0, totalPage: 0, 'reply_content': ''};
    this.loadData();
  }

  loadData() {
    if (this.state.currentPage <= this.state.totalPage) {
      getConsults("", this.state.currentPage++, 15).then(result => {
        if (result.code == 0) {
          let consults = this.state.consults;
          this.state.totalPage = Math.ceil(result.totalCount / 15);
          consults.extend(result.list);
          this.setState({consults});
        }
      });
    }
  }

  handleScroll(event) {
    let wholeHeight=event.currentTarget.scrollHeight;
    let scrollTop=event.currentTarget.scrollTop;
    let divHeight=event.currentTarget.clientHeight;
    if(divHeight+scrollTop>=wholeHeight) {//加载更多数据
      this.loadData();
    }
  }

  handleClick(id) {
    this.props.showPrompt(null, data => {
      if (data != null) {
        let reply = this.state['reply_content'].trim();
        if (reply.length == 0) {
          showToast("请输入回复内容");
        } else {
          putReply(id, reply).then(result => {
            if (result.code == 0) {
              showToast("回复成功");
              this.state = {consults:[], currentPage: 0, totalPage: 0, 'reply_content': ''};
              this.loadData();
            }
          })
        }
      }
      return true;
    });
  }

  render() {//
    return (
      <Container scrollable onScroll={this.handleScroll.bind(this)}>
        <List className="ask-list margin-0 base-background">
          {
            this.state.consults.map(item => {
              return (
                <List.Item key={item.id} className="white-background margin-sm" onClick={this.handleClick.bind(this, item.id)}>
                  <Container direction="column">
                    <div className="flex-row"><h4 className="flex-1">{item.userName||'匿名用户'+item.userId}的提问</h4><label>{XUtil.convertDate(new Date(item.createtime*1000), "YYYY-MM-DD hh:mm")}</label></div>
                    <div className="content">{item.content}</div>
                    <div className="flex-row"><h4 className="flex-1">回复:</h4><label>{item.replytime?XUtil.convertDate(new Date(item.replytime*1000), "YYYY-MM-DD hh:mm"):""}</label></div>
                    <div className="content">{item.reply||'暂无回答'}</div>
                  </Container>
                </List.Item>
              )
            })
          }
        </List>
        <PromptDialog
          title="咨询回复"
          isOpen={false}>
          <Field value="" className="base-hide"/>
          <Field type="textarea" placeholder="请输入回复内容" onChange={this.defaultHandleChange.bind(this,'reply_content')}/>
        </PromptDialog>
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,showPrompt}, dispatch))(Consults);
