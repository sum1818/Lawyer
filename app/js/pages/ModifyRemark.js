import PageBase from "./PageBase";
import React from "react";
import {Container, Group, Field} from "amazeui-touch";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle, setMenu} from "../actions/ActionCreator";
import * as userApi from "../apis/UserApi";
import showToast from "../utils/ToastUtil";

class ModifyRemark extends PageBase {

  constructor() {
    super();
  }

  componentWillMount() {
    this.state = this.props.location.state;
    this.props.setTitle("修改备注");
    this.props.setMenu("保存", null, () => {
      let remark = this.state['name_input'];
      if (remark == null || remark.trim().length == 0) {
        showToast("备注不能为空");
        return;
      }
      userApi.modifyRemark(this.state.id, remark)
        .then(data => {
          if (data.code == 0) {//修改名称成功
            showToast(data.result);
            this.props.setMenu(null, null);
            history.back();
          } else {
            showToast(data.error);
          }
        });
    });
  }

  componentWillUnmount() {
    this.props.setMenu(null, null);
    super.componentWillUnmount();
  }

  render() {
    return (
      <Container>
        <Group component="span">
          <Field
            label="请输入新的备注"
            onChange={this.defaultHandleChange.bind(this, "name_input")}
            defaultValue={this.state.remark}
          />
        </Group>
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu}, dispatch))(ModifyRemark);
