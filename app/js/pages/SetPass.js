import PageBase from './PageBase';
import React from "react";
import {
  Container,
  Group,
  Field,
  Button,
  Icon
} from 'amazeui-touch';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {setTitle,setMenu, showAlert} from "../actions/ActionCreator";
import * as userApi from "../apis/UserApi";
import * as XUtil from "../utils/XUtil";
import showToast from "../utils/ToastUtil";
import AlertDialog from "../dialogs/AlertDialog";

class SetPass extends PageBase {

  constructor() {
    super();
    this.state = {
      code:'',
      remain: 60,
      sendDisabled: false,
      codeTip:'发送验证码'
    }
  }

  componentWillMount() {
    this.props.setTitle("重置密码");
  }
  componentWillUnmount() {
    clearInterval(this.state.left);
    super.componentWillUnmount();
    // this.props.setMenu(null, null);
  }

  descCounter() {
    this.state.remain --;
    if (this.state.remain <= 0) {
      clearInterval(this.state.left);
      this.setState({codeTip:"发送验证码", sendDisabled:false});
    } else {
      this.setState({codeTip:`${this.state.remain}秒后重新发送`, sendDisabled:true});
    }
  }

  handleClick(comId) {
    switch (comId) {
      case 'send_btn':
        userApi.sendResetCode()
          .then(data => {
            if (data.code == 0) {
              //开始60秒倒计时
              clearInterval(this.state.left);
              this.state.remain = 60;
              this.state.left = setInterval(this.descCounter.bind(this), 1000);
              showToast(data.result);
            } else {
              showToast(data.error);
            }
          });
        break;
      case 'reset_btn':
        if (!XUtil.isValidLoginCode(this.state['code_input'])) {
          showToast("请输入正确的验证码");
        } else if (!XUtil.isValidPass(this.state['pass_input'])) {
          showToast("密码应为6-16位的字母和数组组成");
        } else {
          userApi.resetPass(this.state['code_input'], this.state['pass_input'])
            .then(data => {
              if (data.code == 0) {
                showToast("重置密码成功");
                history.back();
              } else {
                showToast(data.error);
              }
            })
        }
        break;
    }
  }

  render() {
    return (
      <Container>
        <Field
          type="text"
          labelBefore={<i className="base-icon-auth" />}
          placeholder="请输入验证码"
          containerClassName="no-arrow login-field"
          onChange={this.defaultHandleChange.bind(this, "code_input")}
          btnAfter={<Button id="send_btn" className="btn-send" disabled={this.state.sendDisabled} onClick={this.handleClick.bind(this, "send_btn")}>{this.state.codeTip}</Button>}
        />
        <Field
          type="password"
          labelBefore={<i className="base-icon-pass" />}
          placeholder="请设置密码(6-16位数字或字母)"
          containerClassName="no-arrow login-field"
          onChange={this.defaultHandleChange.bind(this, "pass_input")}
        />
        <div className="margin-top-lg padding-h-sm">
          <Button amStyle="primary" onClick={this.handleClick.bind(this, "reset_btn")} block>重置</Button>
        </div>
        <AlertDialog
          title="提示"
          isOpen={false}
        />
      </Container>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({setTitle,setMenu,showAlert}, dispatch))(SetPass);
