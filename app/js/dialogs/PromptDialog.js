/**
 * Created by zl on 2017/3/10.
 */
import React from "react";
import {Modal} from "amazeui-touch";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {hidePrompt} from "../actions/ActionCreator";

class PromptDialog extends React.Component {

  closeModal() {
    this.props.hidePrompt();
  }

  render() {
    return (
      <Modal
        role="prompt"
        isOpen={this.props.isOpen}
        onDismiss={this.closeModal.bind(this)}
        onAction={this.props.action}
        title={this.props.title}
      >
        {this.props.children}
      </Modal>
    );
  }
}

export default connect(state=>state.PromptDialogShow, dispatch => bindActionCreators({hidePrompt}, dispatch))(PromptDialog);
