/**
 * Created by zl on 2017/3/10.
 */
import React from "react";
import {
  Modal
} from 'amazeui-touch';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {hideAlert} from '../actions/ActionCreator';

class AlertDialog extends React.Component {

  closeModal() {
    this.props.hideAlert();
  }

  handleAction() {
    this.closeModal();
  }

  render() {
    return (
      <Modal
        role="alert"
        isOpen={this.props.isOpen}
        onDismiss={this.closeModal.bind(this)}
        title={this.props.title}
        onAction={this.handleAction.bind(this)}
      >
        {this.props.children}
      </Modal>
    );
  }
}

export default connect(state=>state.AlertDialogShow, dispatch => bindActionCreators({hideAlert}, dispatch))(AlertDialog);
