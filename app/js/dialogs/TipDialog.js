/**
 * Created by zl on 2017/3/10.
 */
import React from "react";
import {
  Modal
} from 'amazeui-touch';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {hideTips} from '../actions/ActionCreator';

class TipDialog extends React.Component {

  closeModal() {
    this.props.hideTips();
  }

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        onDismiss={this.closeModal.bind(this)}
        title={this.props.title}
      >
        {this.props.children}
      </Modal>
    );
  }
}

export default connect(state=>state.TipDialogShow, dispatch => bindActionCreators({hideTips}, dispatch))(TipDialog);
