// load files
import '../humans.txt';
import '../manifest.json';
import '../manifest.webapp';
import '../robots.txt';

// load images
import '../i/logo.png';
import '../i/favicon.png';
import '../i/touch/apple-touch-icon.png';
import '../i/touch/chrome-touch-icon-192x192.png';
import '../i/touch/icon-128x128.png';
import '../i/touch/ms-touch-icon-144x144-precomposed.png';

// load Amaze UI Touch style
// @see https://github.com/jtangelder/sass-loader#imports
// You can just import it in app.scss
// import 'amazeui-touch/scss/amazeui.touch.scss'

// load style
import '../style/app.scss'
import '../style/native-toast.scss'
import '../style/datepicker.scss'

Array.prototype.extend = function (other_array) {
  other_array.forEach(function(v) {this.push(v)}, this);
};
// load App
import './App'
