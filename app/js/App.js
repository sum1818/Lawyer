import React from "react";
import {render} from "react-dom";
import {Router, Route, Link, IndexRoute, hashHistory} from "react-router";
import {Container, TabBar} from "amazeui-touch";
import CustomTabBar from "./widgets/CustomTabBar";
import {createStore, applyMiddleware, bindActionCreators} from "redux";
import {Provider, connect} from "react-redux";
import ReduxThunk from "redux-thunk";
import {MainReducer} from "./reducer/MainReducer";
import {addMessage, addUnreadCount} from "./actions/ActionCreator";
// Pages
import Index from "./pages/Index";
import Page from "./pages/Page";
import Person from "./pages/Person";
import Ask from "./pages/Ask";
import Consult from "./pages/Consult";
import Message from "./pages/Message";
import PageCase from "./pages/PageCase";
import SearchLawyer from "./pages/SearchLawyer";
import SearchCustomer from "./pages/SearchCustomer";
import SearchCase from "./pages/SearchCase";
import Introduce from "./pages/Introduce";
import Product from "./pages/Product";
import PageLogin from "./pages/PageLogin";
import ProductList from "./pages/ProductList";
import Vip from "./pages/Vip";
import Attention from "./pages/Attention"
import PageSetting from "./pages/PageSetting";
import ConsultPreview from "./pages/ConsultPreview";
import LawyerFee from "./pages/LawyerFee";
import LitigationFee from "./pages/LitigationFee";
import BuyResult from "./pages/BuyResult";
import DateTool from "./pages/DateTool";
import * as XUtil from "./utils/XUtil";
import {getUserInfo} from "./apis/UserApi";
import showTast from './utils/ToastUtil';

class App extends React.Component {

  static contextTypes = {//注入router对象
    router: React.PropTypes.object.isRequired
  };

  constructor(props) {
    super();
    this.state = {};
    getUserInfo();//获取登录状态
    let queryStr = location.search + "";
    let indexCode = queryStr.indexOf("code");
    if (queryStr && indexCode > -1) {//通过微信授权访问
      let params = location.search.substring(indexCode).split("&")
      params.forEach(item => {
        if (item.indexOf("code") > -1) {
          let codeArr = item.split('=');//获取微信返回的Code用来获取用户openId
          let code = codeArr[1];
          XUtil.setItem("wxcode", code);
        }
      })
    }
  }

  render() {
    const {
      location,
      params,
      children,
      ...props,
    } = this.props;
    const {
      router
    } = this.context;
    const transition = children.props.transition || 'fade';
    let show = router.isActive('/', true) || router.isActive('/message', true) || router.isActive('/person', true);
    return (
      <Container direction="column" id="sk-container">
        <Container
          transition={transition}
          // fade transition example
          // transition='fade'
          transitionEnterTimeout={10}
          transitionLeaveTimeout={10}
        >
          {React.cloneElement(children, {key: location.key})}
        </Container>
        {
          !show ? null:(
              <CustomTabBar amStyle="dark">
                <TabBar.Item
                  component={Link}
                  icon="home"
                  title="首页"
                  selected={router.isActive('/', true)}
                  to="/"
                  onlyActiveOnIndex
                />
                <TabBar.Item
                  component={Link}
                  icon="msg"
                  title="消息"
                  badge={this.state.unread>0?this.state.unread:null}
                  selected={router.isActive('/message', true)}
                  to="/message"
                  onlyActiveOnIndex
                />
                <TabBar.Item
                  component={Link}
                  icon="my"
                  title="我的"
                  selected={router.isActive('/person', true)}
                  to="/person"
                  onlyActiveOnIndex
                />
              </CustomTabBar>
            )
        }
      </Container>
    );
  }
}

// withRouter HoC
// @see https://github.com/reactjs/react-router/blob/0616f6e14337f68d3ce9f758aa73f83a255d6db3/upgrade-guides/v2.4.0.md#v240-upgrade-guide

// Store
const store = createStore(MainReducer, applyMiddleware(ReduxThunk));

// Map Redux state to component props
function mapStateToProps(state) {
  return {
    showTab: state.TabsShow,
    imConnect: state.IMConnect,
  };
}

/**
 * 全局登录认证方法
 * @param nextState
 * @param replace
 */
const requireAuth = (nextState, replace, next) => {
  if (XUtil.getItem("isLogin") != "true") {
    XUtil.replaceLogin();
  } else {
    next();//
  }
};

const requireAuth2 = (nextState, replace, next) => {
  if (XUtil.getItem("isLogin") != "true") {
    showTast('请注册后再激活服务卡');
    XUtil.replaceLogin();
  } else {
    next();
  }
};

const Root = connect(mapStateToProps, dispatch => bindActionCreators({addMessage,addUnreadCount}, dispatch))(App);
const routes = (
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={Root}>
        <IndexRoute component={Index} />
        <Route path="/person" component={Person} />
        <Route path="/message" component={Message} />
        <Route path="/pageCase" component={PageCase} onEnter={requireAuth}/>
        <Route path="/searchLawyer" component={SearchLawyer} onEnter={requireAuth}/>
        <Route path="/searchCustomer" component={SearchCustomer} onEnter={requireAuth}/>
        <Route path="/searchCase" component={SearchCase} onEnter={requireAuth}/>
        <Route path="/ask" component={Ask} onEnter={requireAuth}/>
        <Route path="/consult" component={Consult} onEnter={requireAuth}/>
        <Route path="/introduce" component={Introduce}/>
        <Route path="/productList" component={ProductList}/>
        <Route path="/product" component={Product} onEnter={requireAuth}/>
        <Route path="/development" component={Vip} onEnter={requireAuth2}/>
        <Route path="/attention" component={Attention} onEnter={requireAuth}/>
        <Route path="/pageSetting" component={PageSetting} onEnter={requireAuth}/>
        <Route path="/pageLogin" component={PageLogin}/>
        <Route path="/consultPreview" component={ConsultPreview} onEnter={requireAuth}/>
        <Root path="/lawyerFee" component={LawyerFee}/>
        <Root path="/litigationFee" component={LitigationFee}/>
        <Root path="/dateTool" component={DateTool}/>
        <Root path="/buyResult" component={BuyResult}/>
        /** page 不显示tab **/
        <Route path=":page" component={Page} />
      </Route>
    </Router>
  </Provider>
);

document.addEventListener('DOMContentLoaded', () => {
  render(routes, document.getElementById('root'));
});
